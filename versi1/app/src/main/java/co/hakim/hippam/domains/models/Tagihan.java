/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.domains.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class Tagihan implements Parcelable {

    private String idTag;
    private String noPel;
    private String namaPel;
    private String alamatPel;
    private String tagihanBulan;
    private int jumlahTagihan;
    private String tanggalBayar;
    private String statusTagihan;

    public int getSisaSaldo() {
        return sisaSaldo;
    }

    public void setSisaSaldo(int sisaSaldo) {
        this.sisaSaldo = sisaSaldo;
    }

    private int sisaSaldo;

    protected Tagihan(Parcel in) {
        idTag = in.readString();
        noPel = in.readString();
        namaPel = in.readString();
        alamatPel = in.readString();
        tagihanBulan = in.readString();
        jumlahTagihan = in.readInt();
        tanggalBayar = in.readString();
        statusTagihan = in.readString();
        sisaSaldo = in.readInt();
    }

    public Tagihan() {
        //Need Empty Constructor
    }

    public static final Creator<Tagihan> CREATOR = new Creator<Tagihan>() {
        @Override
        public Tagihan createFromParcel(Parcel in) {
            return new Tagihan(in);
        }

        @Override
        public Tagihan[] newArray(int size) {
            return new Tagihan[size];
        }
    };

    public String getIdTag() {
        return idTag;
    }

    public void setIdTag(String idTag) {
        this.idTag = idTag;
    }

    public String getNoPel() {
        return noPel;
    }

    public void setNoPel(String noPel) {
        this.noPel = noPel;
    }

    public String getNamaPel() {
        return namaPel;
    }

    public void setNamaPel(String namaPel) {
        this.namaPel = namaPel;
    }

    public String getAlamatPel() {
        return alamatPel;
    }

    public void setAlamatPel(String alamatPel) {
        this.alamatPel = alamatPel;
    }

    public String getTagihanBulan() {
        return tagihanBulan;
    }

    public void setTagihanBulan(String tagihanBulan) {
        this.tagihanBulan = tagihanBulan;
    }

    public int getJumlahTagihan() {
        return jumlahTagihan;
    }

    public void setJumlahTagihan(int jumlahTagihan) {
        this.jumlahTagihan = jumlahTagihan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idTag);
        dest.writeString(noPel);
        dest.writeString(namaPel);
        dest.writeString(alamatPel);
        dest.writeString(tagihanBulan);
        dest.writeInt(jumlahTagihan);
        dest.writeString(tanggalBayar);
        dest.writeString(statusTagihan);
        dest.writeInt(sisaSaldo);
    }

    public String getTanggalBayar() {
        return tanggalBayar;
    }

    public void setTanggalBayar(String tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public String getStatusTagihan() {
        return statusTagihan;
    }

    public void setStatusTagihan(String statusTagihan) {
        this.statusTagihan = statusTagihan;
    }
}
