package co.hakim.hippam.net;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import java.util.List;

import co.hakim.hippam.Constants;
import cz.msebera.android.httpclient.cookie.Cookie;

/**
 * Created by Hakim on 27-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class RestClient {
    public static final String API_SERVER_ADDR = "https://hippam.com/webservices/";
    public static final String ACTION_URL_LOGIN = "login";
    public static final String ACTION_URL_CUSTOMER = "get_download_pelanggan_list";
    public static final String ACTION_INPUT_SURVEY = "input_meter";
    public static final String ACTION_CHECK_TAGIHAN = "chek_tagihan";
    public static final String ACTION_BAYAR_TAGIHAN = "bayar_tagihan";
    public static final String ACTION_CHECK_SALDO = "check_saldo";
    public static final String ACTION_GET_USER_TAGIHAN = "get_info_tagihan_pelanggan";
    public static final String ACTION_GET_LIST_ADUAN = "get_list_aduan";
    public static final String ACTION_SEND_REPORT = "kirim_aduan";
    public static final String ACTION_GET_DETAIL = "get_pelanggan_detail";
    public static final String ACTION_GET_REKENING = "get_rekening";
    public static final String ACTION_TOP_UP = "topup_saldo";

    private static final String TAG = RestClient.class.getSimpleName();
    private static final String PHP_SESSION_HEADER_NAME = "Cookie";
    private static RestClient instance = null;
    private static PersistentCookieStore cookieStore;
    private AsyncHttpClient asyncHttpClient;
    private SyncHttpClient syncHttpClient;
    private AsyncHttpResponseHandler responseHandler;

    public static RestClient getInstance(Context context,
                                         AsyncHttpResponseHandler responseHandler) {
        if (instance == null) {
            instance = new RestClient();
            cookieStore = new PersistentCookieStore(context);

            instance.asyncHttpClient = new AsyncHttpClient();
            instance.asyncHttpClient.setCookieStore(cookieStore);

            instance.syncHttpClient = new SyncHttpClient();
            instance.syncHttpClient.setCookieStore(cookieStore);
        }

        instance.responseHandler = responseHandler;

        return instance;
    }

    public static void cancelRequests(Context context) {
        if (instance != null) {
            instance.asyncHttpClient.cancelRequests(context, true);
            instance.syncHttpClient.cancelRequests(context, true);
        }
    }

    /*public void addCookie(String name, String value) {
        cookieStore.addCookie(new BasicClientCookie(name, value));
    }*/

    private void get(boolean async, String action, RequestParams params) {
        String uri = API_SERVER_ADDR + action;

        if (Constants.DEBUG) Log.d(TAG, "get request to: " + uri);

        List<Cookie> cookies = cookieStore.getCookies();
        String session = "";
        if (cookies.isEmpty()) {
            if (Constants.DEBUG) Log.d(TAG, "empty cookie");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                session += cookies.get(i).getName() + "=" + cookies.get(i).getValue() + ";";
            }

            if (Constants.DEBUG) Log.d(TAG, "session cookie: " + session);
        }

        if (!session.isEmpty()) {
            if (async) {
                asyncHttpClient.addHeader(PHP_SESSION_HEADER_NAME, session);
            } else {
                syncHttpClient.addHeader(PHP_SESSION_HEADER_NAME, session);
            }
        }

        if (params == null) {
            if (Constants.DEBUG) Log.d(TAG, "no param");

            if (async) {
                asyncHttpClient.get(uri, responseHandler);
            } else {
                syncHttpClient.get(uri, responseHandler);
            }
        } else {
            if (Constants.DEBUG) Log.d(TAG, "param: " + params.toString());
            if (async) {
                asyncHttpClient.get(uri, params, responseHandler);
            } else {
                syncHttpClient.get(uri, params, responseHandler);
            }
        }
    }

    public void post(boolean async, String action, RequestParams params) {
        String uri = API_SERVER_ADDR + action;

        if (Constants.DEBUG) Log.d(TAG, "post request to: " + uri);

        List<Cookie> cookies = cookieStore.getCookies();
        String session = "";
        if (cookies.isEmpty()) {
            Log.d(TAG, "empty cookie");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                session += cookies.get(i).getName() + "=" + cookies.get(i).getValue() + ";";
            }

            if (Constants.DEBUG) Log.d(TAG, "session: " + session);
        }

        if (!session.isEmpty()) {
            if (async) {
                asyncHttpClient.addHeader(PHP_SESSION_HEADER_NAME, session);
            } else {
                syncHttpClient.addHeader(PHP_SESSION_HEADER_NAME, session);
            }
        }

        if (params == null) {
            if (Constants.DEBUG) Log.d(TAG, "no param");
            if (async) {
                asyncHttpClient.post(uri, responseHandler);
            } else {
                syncHttpClient.post(uri, responseHandler);
            }
        } else {
            if (Constants.DEBUG) Log.d(TAG, "param: " + params.toString());
            if (async) {
                asyncHttpClient.post(uri, params, responseHandler);
            } else {
                syncHttpClient.post(uri, params, responseHandler);
            }
        }
    }

    public void postLogin(String username, String password, String idFirebase, boolean async) {
        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("password", password);
        params.put("id_firebase", idFirebase);
        params.put("manufacture", Build.MANUFACTURER);
        params.put("brand", Build.BRAND);
        params.put("os_version", Build.VERSION.SDK_INT);

        post(async, ACTION_URL_LOGIN, params);
    }

    public void getCustomer(String token, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);

        get(async, ACTION_URL_CUSTOMER, params);
    }

    public void postSurvey(String token, String noPel, int meter, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("nopel", noPel);
        params.put("meter", meter);

        post(async, ACTION_INPUT_SURVEY, params);
    }

    public void getTagihan(String idPel, String token, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("nopel", idPel);

        get(async, ACTION_CHECK_TAGIHAN, params);
    }

    public void getSaldo(String token, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);

        get(async, ACTION_CHECK_SALDO, params);
    }

    public void postTagihan(String token, int idTag,  boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("id_tag", idTag);

        post(async, ACTION_BAYAR_TAGIHAN, params);
    }

    public void getTagihanUser(String token, int offset, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("limit", Constants.DATA_FETCH_LENGTH);
        params.put("offset", offset);

        get(async, ACTION_GET_USER_TAGIHAN, params);
    }

    public void postReport(String token, String report,  boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("uraian_aduan", report);

        post(async, ACTION_SEND_REPORT, params);
    }

    public void getListReport(String token, int offset, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("limit", Constants.DATA_FETCH_LENGTH);
        params.put("offset", offset);

        get(async, ACTION_GET_LIST_ADUAN, params);
    }

    public void getListReport(String token, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("limit", Constants.DATA_FETCH_LENGTH);
        params.put("offset", 0);

        get(async, ACTION_GET_LIST_ADUAN, params);
    }

    public void getDetailPelanggan(String token, String id, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);
        params.put("nopel", id);

        get(async, ACTION_GET_DETAIL, params);
    }

    public void getRekening(String token, boolean async){
        RequestParams params = new RequestParams();
        params.put("token_key", token);

        get(async, ACTION_GET_REKENING, params);
    }

    public void postSaldo(RequestParams params,  boolean async){
        post(async, ACTION_TOP_UP, params);
    }
}
