/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.services;

import android.accounts.Account;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import co.hakim.hippam.Authenticator;
import co.hakim.hippam.Constants;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class AuthenticatorService extends Service {
    private static final String TAG = AuthenticatorService.class.getSimpleName();
    private Authenticator mAuthenticator;

    public AuthenticatorService() {
    }

    public static Account getAccount() {
        if (Constants.DEBUG) Log.d(TAG, "getting account for sync");
        final String accountName = Authenticator.ACCOUNT_NAME;
        return (new Account(accountName, Authenticator.ACCOUNT_TYPE));
    }

    @Override
    public void onCreate() {
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
