/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.DateTimeUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by bigio on 11/6/16.
 */

public class HistoryPembayaranAdapter extends RecyclerView.Adapter<HistoryPembayaranAdapter.HistoryPembayaranViewHolder>{

    private static final String TAG = HistoryPembayaranAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private Context context;

    public HistoryPembayaranAdapter(List<Tagihan> tagihanList, Context context) {
        this.tagihanList = tagihanList;
        this.context = context;
    }

    @Override
    public HistoryPembayaranViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryPembayaranViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_history_pembayaran, parent, false));
    }

    @Override
    public void onBindViewHolder(HistoryPembayaranViewHolder holder, int position) {
        Tagihan tagihan = tagihanList.get(position);
        holder.tagPem.setText(String.format("Pembayaran Tagihan %s #%s", tagihan.getTagihanBulan(), tagihan.getIdTag()));
        holder.namaPem.setText(String.format("%s, %s", tagihan.getNoPel(), tagihan.getNamaPel()));
        holder.tagihan.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));
        holder.sisaSaldo.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getSisaSaldo()))).replace(',','.')));

        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(HistoryPembayaranViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(tagihanList != null){
            return tagihanList.size();
        }

        return 0;
    }

    public void swapData(List<Tagihan> tagihanList){
        if(tagihanList != null){
            this.tagihanList = tagihanList;
        }

        notifyDataSetChanged();
    }

    public class HistoryPembayaranViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView tagPem, namaPem, tagihan, sisaSaldo;
        public HistoryPembayaranViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            tagPem = (TextView) itemView.findViewById(R.id.id_pembayaran);
            namaPem = (TextView) itemView.findViewById(R.id.nama_customer);
            tagihan = (TextView) itemView.findViewById(R.id.tagihan);
            sisaSaldo = (TextView) itemView.findViewById(R.id.saldo);
        }
    }
}
