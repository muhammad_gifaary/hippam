/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;
import co.hakim.hippam.fragments.CheckSaldoDialogFragment;
import co.hakim.hippam.fragments.CheckTagihanFragment;
import co.hakim.hippam.fragments.first_version.HistoryPembayaranFragment;
import co.hakim.hippam.utils.UserUtil;

public class LoketActivity extends AppCompatActivity {

    private NavigationPagerAdapter navigationPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loket);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_loket_menu);

        setSupportActionBar(toolbar);

        navigationPagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(navigationPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_loket, menu);
        return true;
    }

    public void checkSaldo(){
        CheckSaldoDialogFragment checkSaldoDialogFragment = new CheckSaldoDialogFragment();
        checkSaldoDialogFragment.show(getSupportFragmentManager(), "Check Saldo Dialog");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (id){
            case R.id.logout:
                UserUtil.getInstance(this).signOut();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.saldo:
                checkSaldo();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class NavigationPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;
        private String[] titles;

        public NavigationPagerAdapter(FragmentManager fm, Context context) {
            super(fm);

            fragments = new ArrayList<>();
            fragments.add(new CheckTagihanFragment());
            fragments.add(new HistoryPembayaranFragment());

            titles = context.getResources().getStringArray(R.array.label_loket);
        }

        @Override
        public Fragment getItem(int position) {
            if (position < 0 || position >= fragments.size()) {
                Toast.makeText(getBaseContext(), "Unknown menu", Toast.LENGTH_SHORT).show();
                return null;
            }

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position < titles.length) {
                return titles[position];
            }

            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
