package co.hakim.hippam.domains.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class Customer implements Parcelable {

    private static final String TAG = Customer.class.getSimpleName();

    private String customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private String meterId;
    private String customerGroup;
    private String customerArea;

    protected Customer(Parcel in) {
        customerId = in.readString();
        customerName = in.readString();
        customerAddress = in.readString();
        customerPhone = in.readString();
        meterId = in.readString();
        customerGroup = in.readString();
        customerArea = in.readString();
    }

    public Customer(){
        //Need Empty Constructor
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerArea() {
        return customerArea;
    }

    public void setCustomerArea(String customerArea) {
        this.customerArea = customerArea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerId);
        dest.writeString(customerName);
        dest.writeString(customerAddress);
        dest.writeString(customerPhone);
        dest.writeString(meterId);
        dest.writeString(customerGroup);
        dest.writeString(customerArea);
    }
}
