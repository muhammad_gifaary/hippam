package co.hakim.hippam.net;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Hakim on 27-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class RestResponseHandler extends JsonHttpResponseHandler {
    private String tag;

    public RestResponseHandler(@NonNull String tag) {
        this.tag = tag;
    }

    public static JSONObject getData(JSONObject response) throws JSONException {
        return response.getJSONObject("data");
    }

    public static boolean isSuccess(JSONObject response) throws JSONException {
        return response.getString("sts").equals("true");
    }

    public void onFailure(String message, Throwable throwable) {
        Log.e(tag + ".Rest", message, throwable);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                          JSONObject errorResponse) {
        if (errorResponse != null) {
            onFailure(errorResponse.toString(), throwable);
        } else {
            onFailure(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                          JSONArray errorResponse) {
        if (errorResponse != null) {
            onFailure(errorResponse.toString(), throwable);
        } else {
            onFailure(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString,
                          Throwable throwable) {
        if (!TextUtils.isEmpty(responseString)) {
            onFailure(responseString, throwable);
        } else {
            onFailure(throwable.getMessage(), throwable);
        }
    }
}
