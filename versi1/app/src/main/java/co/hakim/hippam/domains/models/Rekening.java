/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.domains.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bigio on 11/7/16.
 */

public class Rekening implements Parcelable{
    private int id;
    private String noRekening;

    protected Rekening(Parcel in) {
        id = in.readInt();
        noRekening = in.readString();
    }

    public Rekening() {
    }

    public static final Creator<Rekening> CREATOR = new Creator<Rekening>() {
        @Override
        public Rekening createFromParcel(Parcel in) {
            return new Rekening(in);
        }

        @Override
        public Rekening[] newArray(int size) {
            return new Rekening[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(noRekening);
    }
}
