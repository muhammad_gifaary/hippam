/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;
import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;
import co.hakim.hippam.adapters.CustomerListAdapter;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.providers.HippamContentProviders;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.SyncUtil;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class EnumeratorActivity extends AppCompatActivity implements ItemClickListener{

    private CustomerListAdapter customerListAdapter;
    private RecyclerView customerRecycleView;


    private CustomerTable customerTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enumerator);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.customer_list);

        setSupportActionBar(toolbar);

        customerTable = new CustomerTable(this);
        customerListAdapter = new CustomerListAdapter(this, this, customerTable.getAll());
        customerRecycleView = (RecyclerView) findViewById(R.id.list_user);
        customerRecycleView.setLayoutManager(new LinearLayoutManager(this));
        customerRecycleView.setItemAnimator(new DefaultItemAnimator());
        customerRecycleView.setAdapter(customerListAdapter);

        SyncUtil.createSyncAccount(this);
    }

    @Override
    public void onItemClicked(int position, View view) {
        Customer customer = customerListAdapter.getCustomer(position);

        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putExtra(Constants.EXTRA_CUSTOMER, customer);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            UserUtil.getInstance(this).signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh(){

        customerTable = new CustomerTable(this);
        getContentResolver().delete(HippamContentProviders.URI_CUSTOMERS, null, null);

        JsonHttpResponseHandler customerResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray data = response.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject datum = data.getJSONObject(i);
                            Customer c = new Customer();
                            CustomerTable.setValues(datum, c);
                            customerTable.sync(c);
                        }

                        customerListAdapter.swapData(customerTable.getAll());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(this, customerResponseHandler).getCustomer(UserUtil.getInstance(this).getToken(), true);
    }
}
