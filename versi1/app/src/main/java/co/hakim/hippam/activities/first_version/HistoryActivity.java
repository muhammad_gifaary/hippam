/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.fragments.first_version.DetailCustomerFragment;
import co.hakim.hippam.fragments.HistoryCustomerFragment;

public class HistoryActivity extends AppCompatActivity {

    private NavigationPagerAdapter navigationPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.customer_detail);

        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Customer customer = getIntent().getParcelableExtra(Constants.EXTRA_CUSTOMER);

        navigationPagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager(), this, customer);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(navigationPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()){
            case android.R.id.home :
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class NavigationPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;
        private String[] titles;

        public NavigationPagerAdapter(FragmentManager fm, Context context, Customer customer) {
            super(fm);

            fragments = new ArrayList<>();
            fragments.add(DetailCustomerFragment.newInstance(customer));
            fragments.add(HistoryCustomerFragment.newInstance(customer));

            titles = context.getResources().getStringArray(R.array.label_detail_pelanggan);
        }

        @Override
        public Fragment getItem(int position) {
            if (position < 0 || position >= fragments.size()) {
                Toast.makeText(getBaseContext(), "Unknown menu", Toast.LENGTH_SHORT).show();
                return null;
            }

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position < titles.length) {
                return titles[position];
            }

            return null;
        }
    }
}
