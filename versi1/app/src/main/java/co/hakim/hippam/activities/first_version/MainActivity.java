/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button loginPetugas, loginLoket, loginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginPetugas = (Button) findViewById(R.id.login_petugas);
        loginLoket = (Button) findViewById(R.id.login_loket);
        loginUser = (Button) findViewById(R.id.login_pengguna);

        loginPetugas.setOnClickListener(this);
        loginLoket.setOnClickListener(this);
        loginUser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_petugas:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.login_loket:
                break;
            case R.id.login_pengguna:
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
