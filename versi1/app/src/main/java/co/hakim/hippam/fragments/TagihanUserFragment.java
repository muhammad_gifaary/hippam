/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.TagihanPembayaranAdapter;
import co.hakim.hippam.adapters.TagihanUserListAdapter;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagihanUserFragment extends Fragment implements DataLoadListener{

    private static final String TAG = TagihanUserFragment.class.getSimpleName();

    private int offset = 0;
    private TagihanPembayaranAdapter tagihanPembayaranAdapter;
    private List<Tagihan> tagihanList;
    private ImageView notFound;
    private RecyclerView listTagihanRecyclerView;
    private Context context;


    public TagihanUserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tagihan_user, container, false);

        notFound = (ImageView)  view.findViewById(R.id.not_found);

        listTagihanRecyclerView = (RecyclerView) view.findViewById(R.id.list_tagihan);
        tagihanPembayaranAdapter = new TagihanPembayaranAdapter(null, context, this);
        listTagihanRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        listTagihanRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listTagihanRecyclerView.setAdapter(tagihanPembayaranAdapter);

        return view;
    }

    @Override
    public void onDataFetchStarted() {

    }

    @Override
    public void onDataFetchFinished() {

    }

    @Override
    public void onLastData() {
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        offset = 0;
        loadData();
    }

    private void loadData(){
        if (offset < 0) {
            return;
        }

        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("tagihan");
                        int len = datum.length();

                        if(offset == 0){
                            if(tagihanPembayaranAdapter.getItemCount() > 0){
                                tagihanPembayaranAdapter.clearItems();
                            }
                        }

                        if (len == Constants.DATA_FETCH_LENGTH) {
                            offset += len;
                        } else {
                            offset = -1;
                        }

                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Tagihan tagihan = new Tagihan();
                            tagihan.setIdTag(data.getString("id_tag"));
                            tagihan.setTagihanBulan(data.getString("tagihan"));
                            tagihan.setJumlahTagihan(data.getInt("jumlah"));
                            tagihan.setStatusTagihan(data.getString("status"));

                            tagihanPembayaranAdapter.add(tagihan);
                            //tagihanList.add(tagihan);
                        }

                        //tagihanPembayaranAdapter.swapData(tagihanList);
                        if(tagihanPembayaranAdapter.getItemCount() > 0) {
                            listTagihanRecyclerView.setVisibility(View.VISIBLE);
                        }
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();

                if (tagihanPembayaranAdapter.getItemCount() > 0 &&
                        tagihanPembayaranAdapter.getItemViewType(tagihanPembayaranAdapter.getItemCount() - 1) ==
                                TagihanUserListAdapter.VIEW_PROGRESS) {
                    tagihanPembayaranAdapter.remove(tagihanPembayaranAdapter.getItemCount() - 1);
                }
            }
        };

        RestClient.getInstance(context, tagihanResponseHandler).getTagihanUser(UserUtil.getInstance(context).getToken(), offset , true);
    }
}
