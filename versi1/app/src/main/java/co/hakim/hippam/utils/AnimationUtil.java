package co.hakim.hippam.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import co.hakim.hippam.R;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class AnimationUtil {
    public static void fadeIn(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        animation.setDuration(
                context.getResources().getInteger(android.R.integer.config_longAnimTime));
        view.startAnimation(animation);
    }

    public static void fadeOut(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out);
        animation.setDuration(
                context.getResources().getInteger(android.R.integer.config_longAnimTime));
        view.startAnimation(animation);
    }

    public static void slideUpIn(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up_in);
        animation.setDuration(
                context.getResources().getInteger(android.R.integer.config_longAnimTime));
        view.startAnimation(animation);
    }

    public static void slideDownOut(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_down_out);
        animation.setDuration(
                context.getResources().getInteger(android.R.integer.config_longAnimTime));
        view.startAnimation(animation);
    }

    public static void slideRightToLeft(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_right_to_left);
        animation.setDuration(700);
        view.startAnimation(animation);
    }

    public static void slideLeftToRight(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_left_to_right);
        animation.setDuration(
                context.getResources().getInteger(android.R.integer.config_longAnimTime));
        view.startAnimation(animation);
    }
}
