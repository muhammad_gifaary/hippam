package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.utils.AnimationUtil;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListViewHolder> {

    private static final String TAG = CustomerListAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Customer> customerList;
    private ItemClickListener itemClickListener;

    private Context context;

    public CustomerListAdapter(ItemClickListener itemClickListener, Context context, List<Customer> customerList) {
        this.itemClickListener = itemClickListener;
        this.context = context;
        this.customerList = customerList;
    }

    @Override
    public CustomerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomerListViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_customer, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(CustomerListViewHolder holder, int position) {
        Customer customer = customerList.get(position);

        holder.nomor.setText(customer.getCustomerId());
        holder.name.setText(customer.getCustomerName());
        holder.address.setText(customer.getCustomerAddress());

        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(CustomerListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(customerList != null){
            return customerList.size();
        }

        return 0;
    }

    public void swapData(List<Customer> customerList){
        if(customerList != null){
            this.customerList = customerList;
        }

        notifyDataSetChanged();
    }

    public Customer getCustomer(int position){
        return customerList.get(position);
    }

    public class CustomerListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView nomor, name, address;
        public CustomerListViewHolder(final View itemView, final ItemClickListener itemClickListener) {
            super(itemView);
            container = (View) itemView.findViewById(R.id.container);
            nomor = (TextView) itemView.findViewById(R.id.nomor);
            name = (TextView) itemView.findViewById(R.id.name);
            address = (TextView) itemView.findViewById(R.id.address);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        itemClickListener.onItemClicked(getAdapterPosition(), itemView);
                    }
                }
            });
        }
    }
}
