/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.DateTimeUtil;
import co.hakim.hippam.utils.UserUtil;

/**
 * Created by Hakim on 04-Nov-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */

public class PencatatListAdapter extends RecyclerView.Adapter<PencatatListAdapter.PencatatListViewHolder> {

    private static final String TAG = SurveyListAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Survey> surveyList;
    private CustomerTable customerTable;
    private SurveyTable surveyTable;

    private Context context;

    public PencatatListAdapter(List<Survey> surveyList, Context context) {
        this.surveyList = surveyList;
        this.context = context;
        this.customerTable = new CustomerTable(context);
        this.surveyTable = new SurveyTable(context);
    }

    @Override
    public PencatatListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PencatatListViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_pencatat_meter, parent, false));
    }

    @Override
    public void onBindViewHolder(PencatatListViewHolder holder, int position) {
        Survey survey = surveyList.get(position);
        Customer customer = customerTable.getById(survey.getCustomerId());

        holder.name.setText(customer.getCustomerName());
        holder.address.setText(customer.getCustomerAddress());
        holder.id.setText(String.valueOf(customer.getCustomerId()));
        holder.group.setText(customer.getCustomerGroup());

        int color;
        if(survey.getSyncStatus().equals(Constants.SYNC_STATUS_PENDING)){
            color = ContextCompat.getColor(context, R.color.black);
        }else{
            color = ContextCompat.getColor(context, R.color.sea_blue);
        }

        Date date = DateTimeUtil.monthAgo(1);

        Survey surveyAgo = surveyTable.getByIdAndDate(customer.getCustomerId(), date);
        int diff = survey.getInputMeter();
        int oldMeter = 0;
        if(surveyAgo != null){
            oldMeter = surveyAgo.getInputMeter();
            diff = survey.getInputMeter() - surveyAgo.getInputMeter();
        }

        holder.containerMeter.setBackgroundColor(color);
        holder.meterNow.setText(String.valueOf(survey.getInputMeter()));
        holder.meterLalu.setText(String.valueOf(oldMeter));
        holder.differentMeter.setText(String.valueOf(diff));

        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(PencatatListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(surveyList != null){
            if(customerTable.getAll().size() > 0){
                return surveyList.size();
            }
        }

        return 0;
    }

    public void swapData(List<Survey> listSurvey){
        if(surveyList != null){
            surveyList = listSurvey;
        }

        notifyDataSetChanged();
    }

    public void add(Survey survey){
        if(surveyList != null){
            surveyList = new ArrayList<>();
        }

        surveyList.add(survey);
        notifyItemInserted(surveyList.size() - 1);
    }

    public class PencatatListViewHolder extends RecyclerView.ViewHolder{

        private View container, containerMeter;
        private TextView name, address, id, group, meterNow, meterLalu, differentMeter;
        public PencatatListViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            containerMeter = itemView.findViewById(R.id.container_different_meter);
            name = (TextView) itemView.findViewById(R.id.customer_name);
            address = (TextView) itemView.findViewById(R.id.customer_address);
            id = (TextView) itemView.findViewById(R.id.customer_id);
            group = (TextView) itemView.findViewById(R.id.customer_group);
            meterNow = (TextView) itemView.findViewById(R.id.meter_sekarang);
            meterLalu = (TextView) itemView.findViewById(R.id.meter_lalu);
            differentMeter = (TextView) itemView.findViewById(R.id.different_meter);
        }
    }
}
