/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by Hakim on 30-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class TagihanUserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = HistoryTagihanAdapter.class.getSimpleName();

    public static final int VIEW_PROGRESS = 0;
    public static final int VIEW_ITEM = 1;
    private DataLoadListener loadListener;
    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private Context context;

    public TagihanUserListAdapter(Context context, List<Tagihan> tagihanList, DataLoadListener dataLoadListener) {
        this.context = context;
        this.tagihanList = tagihanList;
        this.loadListener = dataLoadListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        if (viewType == VIEW_ITEM) {
            if (Constants.DEBUG) Log.d(TAG, "item row");
            viewHolder = new TagihanUserListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tagihan_user, parent, false));
        } else {
            if (Constants.DEBUG) Log.d(TAG, "progress row");
            View rootView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_progress, parent, false);
            viewHolder = new ProgressBarViewHolder(rootView);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vHolder, int position) {
        View container;
        if(vHolder instanceof TagihanUserListViewHolder){
            TagihanUserListViewHolder holder = (TagihanUserListViewHolder) vHolder;
            Tagihan tagihan = tagihanList.get(position);
            holder.tagihanBulan.setText(tagihan.getTagihanBulan());
            holder.jumlahTagihan.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));

            if(tagihan.getStatusTagihan().equals("Belum")){
                holder.statusPembayaran.setText("Belum Lunas");
                holder.statusPembayaran.setTextColor(ContextCompat.getColor(context, R.color.red));
            }else{
                holder.statusPembayaran.setText("Lunas");
                holder.statusPembayaran.setTextColor(ContextCompat.getColor(context, R.color.limegreen));
            }

            container = holder.container;

            if (position == getItemCount() - 2 && loadListener != null) {
                loadListener.onLastData();
            }
        }else{
            ProgressBarViewHolder viewHolder = (ProgressBarViewHolder) vHolder;
            container = viewHolder.getContainer();
        }


        setAnimation(container, position);
    }
    public void onViewDetachedFromWindow(TagihanUserListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return tagihanList.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    @Override
    public int getItemCount() {
        if(tagihanList != null){
            return tagihanList.size();
        }

        return 0;
    }

    public void swapData(List<Tagihan> tagihanList){
        if(tagihanList != null){
            this.tagihanList = tagihanList;
        }

        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (tagihanList != null) {
            tagihanList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public class TagihanUserListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView tagihanBulan, jumlahTagihan, statusPembayaran;

        public TagihanUserListViewHolder(View itemView) {
            super(itemView);
            container = (View) itemView.findViewById(R.id.container);
            tagihanBulan = (TextView) itemView.findViewById(R.id.bulan_tagihan);
            jumlahTagihan = (TextView) itemView.findViewById(R.id.jumlah);
            statusPembayaran = (TextView) itemView.findViewById(R.id.status);
        }
    }

    public class ProgressBarViewHolder extends RecyclerView.ViewHolder{
        private ProgressBar progressBar;
        private View container;

        public ProgressBarViewHolder(View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.container);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }

        public View getContainer() {
            return container;
        }

        public ProgressBar getProgressBar() {
            return progressBar;
        }
    }
}
