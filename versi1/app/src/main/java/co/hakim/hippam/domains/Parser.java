package co.hakim.hippam.domains;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class Parser {
    private static final String TAG = Parser.class.getSimpleName();

    public static int getInt(JSONObject source, String name) {
        try {
            return source.getInt(name);
        } catch (JSONException e) {
            //Log.d(TAG, e.getMessage(), e);
        }


        try {
            return Integer.parseInt(source.getString(name));
        } catch (NumberFormatException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }


        return 0;
    }

    public static String getString(JSONObject source, String name) {
        try {
            Log.d(TAG, String.format("retrieving %s, value is: %s", name, source.getString(name)));
            return source.getString(name);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }

    public static double getDouble(JSONObject source, String name) {
        try {
            return source.getDouble(name);
        } catch (JSONException e) {
            //Log.d(TAG, e.getMessage(), e);
        }

        try {
            return Double.parseDouble(source.getString(name));
        } catch (NumberFormatException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return 0.0;
    }
}
