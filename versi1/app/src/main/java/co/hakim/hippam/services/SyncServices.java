/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import co.hakim.hippam.adapters.SyncAdapter;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class SyncServices extends Service {
    private static final Object syncAdapterLock = new Object();
    private static SyncAdapter syncAdapter;

    public SyncServices() {
    }

    @Override
    public void onCreate() {
        synchronized (syncAdapterLock) {
            if (syncAdapter == null)
                syncAdapter = new SyncAdapter(getApplicationContext(), true);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
