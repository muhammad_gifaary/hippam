/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.utils.AnimationUtil;

/**
 * Created by Hakim on 01-Nov-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class ReportListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final String TAG = TagihanListAdapter.class.getSimpleName();

    public static final int VIEW_PROGRESS = 0;
    public static final int VIEW_ITEM = 1;
    private DataLoadListener loadListener;
    private int lastPosition = -1;
    private List<Report> reportList;

    private Context context;

    public ReportListAdapter(List<Report> reportList, Context context, DataLoadListener dataLoadListener) {
        this.reportList = reportList;
        this.context = context;
        this.loadListener = dataLoadListener;
    }

    public ReportListAdapter(List<Report> reportList, Context context) {
        this.reportList = reportList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        if (viewType == VIEW_ITEM) {
            if (Constants.DEBUG) Log.d(TAG, "item row");
            viewHolder =  new ReportListViewHolder(LayoutInflater.from(
                    parent.getContext()).inflate(R.layout.row_laporan, parent, false));
        } else {
            if (Constants.DEBUG) Log.d(TAG, "progress row");
            View rootView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_progress, parent, false);
            viewHolder = new ProgressBarViewHolder(rootView);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vholder, int position) {
        View container;

        if(vholder instanceof ReportListViewHolder) {
            Report report = reportList.get(position);

            ReportListViewHolder holder = (ReportListViewHolder) vholder;
            holder.progress.setText(report.getProgress());
            holder.aduan.setText(report.getReport());

            if (position == getItemCount() - 2 && loadListener != null) {
                loadListener.onLastData();
            }

            container = holder.container;
        }else{
            ProgressBarViewHolder viewHolder = (ProgressBarViewHolder) vholder;
            container = viewHolder.getContainer();
        }

        setAnimation(container, position);
    }

    @Override
    public int getItemViewType(int position) {
        return reportList.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if (reportList == null) {
            return 0;
        }

        return reportList.size();
    }

    public void swapData(List<Report> reportList){
        if(reportList != null){
            this.reportList = reportList;
        }

        notifyDataSetChanged();
    }

    public void add(Report report){
        if(reportList == null){
            reportList = new ArrayList<>();
        }

        Log.d(TAG, "Jumlah " + reportList.size());
        reportList.add(report);
        notifyItemInserted(reportList.size() - 1);
    }

    public void clearItems() {
        if (Constants.DEBUG) Log.d(TAG, "clearing items");
        if (reportList != null) {
            reportList.clear();
            notifyDataSetChanged();
        }
    }

    public void remove(int position) {
        if (reportList != null) {
            reportList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public class ReportListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView aduan, progress, date, time;
        public ReportListViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            aduan = (TextView) itemView.findViewById(R.id.report);
            progress = (TextView) itemView.findViewById(R.id.status);
            date = (TextView) itemView.findViewById(R.id.date_report);
            time = (TextView) itemView.findViewById(R.id.time_report);
        }
    }
}
