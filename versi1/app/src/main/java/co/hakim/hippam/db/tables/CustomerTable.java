package co.hakim.hippam.db.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.domains.Parser;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.providers.HippamContentProviders;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class CustomerTable {
    private static final String TAG = CustomerTable.class.getSimpleName();

    public static final String TABLE_NAME = "customer";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CUSTOMER_ID = "_customer_id";
    public static final String COLUMN_NAME = "_name";
    public static final String COLUMN_ADDRESS = "_address";
    public static final String COLUMN_PHONE = "_phone";
    public static final String COLUMN_METER_NUMBER = "_meter_id";
    public static final String COLUMN_GROUP = "_group";
    public static final String COLUMN_AREA = "_area";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, COLUMN_CUSTOMER_ID, COLUMN_NAME, COLUMN_ADDRESS, COLUMN_PHONE, COLUMN_METER_NUMBER,
            COLUMN_GROUP, COLUMN_AREA
    };

    private static final Uri URI = HippamContentProviders.URI_CUSTOMERS;
    private static final String CREATE_TABLE;

    static {
        CREATE_TABLE = new StringBuilder().append("create table ")
                .append(TABLE_NAME)
                .append(" (")
                .append(COLUMN_ID)
                .append(" integer primary key autoincrement, ")
                .append(COLUMN_CUSTOMER_ID)
                .append(" text not null, ")
                .append(COLUMN_NAME)
                .append(" text not null, ")
                .append(COLUMN_ADDRESS)
                .append(" text not null, ")
                .append(COLUMN_PHONE)
                .append(" text not null, ")
                .append(COLUMN_METER_NUMBER)
                .append(" text not null, ")
                .append(COLUMN_GROUP)
                .append(" text not null, ")
                .append(COLUMN_AREA)
                .append(" text not null)")
                .toString();
    }

    private Context context;

    public CustomerTable(Context context) {
        this.context = context;
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public static void setValues(Cursor cursor, Customer customer) {
        customer.setCustomerId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CUSTOMER_ID)));
        customer.setCustomerName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)));
        customer.setCustomerAddress(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ADDRESS)));
        customer.setCustomerPhone(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PHONE)));
        customer.setMeterId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_METER_NUMBER)));
        customer.setCustomerGroup(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_GROUP)));
        customer.setCustomerArea(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AREA)));
    }

    public static void setValues(Customer customer, ContentValues values) {
        values.put(COLUMN_CUSTOMER_ID, customer.getCustomerId());
        values.put(COLUMN_NAME, customer.getCustomerName());
        values.put(COLUMN_ADDRESS, customer.getCustomerAddress());
        values.put(COLUMN_PHONE, customer.getCustomerPhone());
        values.put(COLUMN_METER_NUMBER, customer.getMeterId());
        values.put(COLUMN_GROUP, customer.getCustomerGroup());
        values.put(COLUMN_AREA, customer.getCustomerArea());
    }

    public static void setValues(JSONObject source, Customer target) {
        target.setCustomerId(Parser.getString(source, "nopel"));
        target.setCustomerName(Parser.getString(source, "nama"));
        target.setCustomerAddress(Parser.getString(source, "alamat"));
        target.setCustomerPhone(Parser.getString(source, "telp"));
        target.setMeterId(Parser.getString(source, "no_meter"));
        target.setCustomerGroup(Parser.getString(source, "gol"));
        target.setCustomerArea(Parser.getString(source, "area"));
    }

    public void sync(Customer customer) {
        Customer c = getById(customer.getCustomerId());
        if (c == null) {
            insert(customer);
        } else {
            update(customer);
        }
    }

    public void insert(Customer customer) {
        if (Constants.DEBUG) {
            Log.d(TAG, "insert customer");
            Log.d(TAG, "data = " + customer.getCustomerId() + " " + customer.getCustomerName());
        }

        ContentValues values = new ContentValues();
        setValues(customer, values);
        context.getContentResolver().insert(URI, values);
    }

    public void update(Customer customer) {
        if (Constants.DEBUG) {
            Log.d(TAG, "updateById, id = " + customer.getCustomerId());
        }

        ContentValues values = new ContentValues();
        setValues(customer, values);
        Uri uri = Uri.parse(URI + "/" + customer.getCustomerId());
        context.getContentResolver().update(uri, values, null, null);
    }

    public Customer getById(String customerId) {

        if (Constants.DEBUG) {
            Log.d(TAG, "getByNumberId, id = " + customerId);
        }

        Uri uri = Uri.parse(URI + "/" + customerId);
        Cursor c = context.getContentResolver().query(uri, ALL_COLUMNS, null, null, null);

        if (c != null && c.moveToFirst()) {
            Customer cs = new Customer();
            setValues(c, cs);
            c.close();
            return cs;
        }

        return null;
    }

    public boolean isEmpty() {

        Cursor c = context.getContentResolver().query(URI, new String[]{CustomerTable.COLUMN_CUSTOMER_ID}, null, null, null);
        if (c == null)
            return true;

        if (c.moveToFirst()) {
            c.close();
            return false;
        }

        return true;
    }

    public static List<Customer> cursorToCustomerList(Cursor cursor) {
        List<Customer> customerList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Customer customer = new Customer();
            setValues(cursor, customer);
            customerList.add(customer);
            cursor.moveToNext();
        }

        return customerList;
    }

    public List<Customer> getAll() {

        List<Customer> customerList = new ArrayList<>();

        Cursor c = context.getContentResolver().query(URI, ALL_COLUMNS, null, null, COLUMN_CUSTOMER_ID);
        if (c == null)
            return customerList;

        if (c.moveToFirst()) {
            do {
                Customer cs = new Customer();
                setValues(c, cs);
                customerList.add(cs);
            } while (c.moveToNext());
        }
        c.close();

        return customerList;
    }

}
