/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.utils.AnimationUtil;

/**
 * Created by bigio on 11/6/16.
 */

public class InformationAdapter extends RecyclerView.Adapter<InformationAdapter.InformationListViewHolder> {
    private static final String TAG = TagihanListAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<String> informationList;

    private Context context;

    public InformationAdapter(List<String> informationList, Context context) {
        this.informationList = informationList;
        this.context = context;
    }

    @Override
    public InformationListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InformationListViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_information, parent, false));
    }

    @Override
    public void onBindViewHolder(InformationListViewHolder holder, int position) {

        holder.information.setText(informationList.get(position));
        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(InformationListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(informationList != null){
            return informationList.size();
        }

        return 0;
    }

    public void swapData(List<String> informationList){
        if(informationList != null){
            this.informationList = informationList;
        }

        notifyDataSetChanged();
    }

    public class InformationListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView information;
        public InformationListViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            information = (TextView) itemView.findViewById(R.id.aduan);
        }
    }
}
