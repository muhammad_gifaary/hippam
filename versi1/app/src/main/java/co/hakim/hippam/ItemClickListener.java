package co.hakim.hippam;

import android.view.View;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public interface ItemClickListener {
    void onItemClicked(int position, View view);
}
