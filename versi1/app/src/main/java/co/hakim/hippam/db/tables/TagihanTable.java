/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.db.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.domains.providers.HippamContentProviders;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * Created by Hakim on 30-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class TagihanTable {
    private static final String TAG = TagihanTable.class.getSimpleName();

    public static final String TABLE_NAME = "_tagihan";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CUSTOMER_ID = "_customer_id";
    public static final String COLUMN_TAG = "_tag";
    public static final String COLUMN_NAME = "_name";
    public static final String COLUMN_ADDRESS = "_address";
    public static final String COLUMN_TAGIHAN = "_tagihan";
    public static final String COLUMN_JUMLAH = "_jumlah";
    public static final String COLUMN_DATE = "_date";
    public static final String COLUMN_SISA_SALDO = "_sisa_saldo";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, COLUMN_CUSTOMER_ID, COLUMN_TAG, COLUMN_NAME, COLUMN_ADDRESS, COLUMN_TAGIHAN, COLUMN_JUMLAH, COLUMN_DATE, COLUMN_SISA_SALDO
    };

    private static final Uri URI = HippamContentProviders.URI_TAGIHAN;
    private static final String CREATE_TABLE;

    static {
        CREATE_TABLE = new StringBuilder().append("create table ")
                .append(TABLE_NAME)
                .append(" (")
                .append(COLUMN_ID)
                .append(" integer primary key autoincrement, ")
                .append(COLUMN_CUSTOMER_ID)
                .append(" text not null, ")
                .append(COLUMN_TAG)
                .append(" integer not null, ")
                .append(COLUMN_NAME)
                .append(" text not null, ")
                .append(COLUMN_ADDRESS)
                .append(" text not null, ")
                .append(COLUMN_TAGIHAN)
                .append(" text not null, ")
                .append(COLUMN_JUMLAH)
                .append(" text not null, ")
                .append(COLUMN_SISA_SALDO)
                .append(" text not null, ")
                .append(COLUMN_DATE)
                .append(" text not null)")
                .toString();
    }

    private Context context;

    public TagihanTable(Context context) {
        this.context = context;
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public static void setValues(Cursor cursor, Tagihan tagihan) {
        tagihan.setNoPel(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CUSTOMER_ID)));
        tagihan.setIdTag(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TAG)));
        tagihan.setNamaPel(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)));
        tagihan.setAlamatPel(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ADDRESS)));
        tagihan.setTagihanBulan(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TAGIHAN)));
        tagihan.setJumlahTagihan(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_JUMLAH)));
        tagihan.setSisaSaldo(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_SISA_SALDO)));
    }

    public static void setValues(Tagihan tagihan, ContentValues values) {
        values.put(COLUMN_CUSTOMER_ID, tagihan.getNoPel());
        values.put(COLUMN_TAG, tagihan.getIdTag());
        values.put(COLUMN_NAME, tagihan.getNamaPel());
        values.put(COLUMN_ADDRESS, tagihan.getAlamatPel());
        values.put(COLUMN_TAGIHAN, tagihan.getTagihanBulan());
        values.put(COLUMN_JUMLAH, tagihan.getJumlahTagihan());
        values.put(COLUMN_DATE, DateTimeUtil.formatDBDateOnly(DateTimeUtil.now()));
        values.put(COLUMN_SISA_SALDO, tagihan.getSisaSaldo());
    }

    public void insert(Tagihan tagihan) {
        if (Constants.DEBUG) {
            Log.d(TAG, "insert tagihan");
            Log.d(TAG, "data = " + tagihan.getJumlahTagihan() + " " + tagihan.getTagihanBulan());
        }

        ContentValues values = new ContentValues();
        setValues(tagihan, values);
        context.getContentResolver().insert(URI, values);
    }

    public List<Tagihan> getAll() {

        List<Tagihan> tagihanList = new ArrayList<>();

        Cursor c = context.getContentResolver().query(URI, ALL_COLUMNS, null, null, COLUMN_CUSTOMER_ID);
        if (c == null)
            return tagihanList;

        if (c.moveToFirst()) {
            do {
                Tagihan t = new Tagihan();
                setValues(c, t);
                tagihanList.add(t);
            } while (c.moveToNext());
        }
        c.close();

        return tagihanList;
    }

}
