/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam;

/**
 * Created by Hakim on 30-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public interface DataLoadListener {
    void onDataFetchStarted();

    void onDataFetchFinished();

    void onLastData();
}
