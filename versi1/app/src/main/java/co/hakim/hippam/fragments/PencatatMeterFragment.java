/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.PencatatListAdapter;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class PencatatMeterFragment extends Fragment {

    private static final String TAG = PencatatMeterFragment.class.getSimpleName();
    public static final int DIALOG_FRAGMENT = 1;

    private PencatatListAdapter pencatatListAdapter;
    private SurveyTable surveyTable;
    private Context context;
    private LinearLayout linearLayout;
    private RecyclerView recyclerView;
    private List<Survey> surveyList;

    public PencatatMeterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        surveyTable = new SurveyTable(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pencatat_meter, container, false);

        Spinner filter = (Spinner) view.findViewById(R.id.month_filter);

        List<String> monthList = new ArrayList<>();
        if(surveyTable.getGroupSurvey().size() > 0){
            for (Survey s : surveyTable.getGroupSurvey()){
                monthList.add(DateTimeUtil.format(s.getInputDate(), Constants.DATE_MONTH_YEAR));
            }
        }else{
            monthList.add(DateTimeUtil.format(DateTimeUtil.now(), Constants.DATE_MONTH_YEAR));
        }

        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, monthList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        filter.setAdapter(adapter);


        surveyList = surveyTable.getAll();
        recyclerView = (RecyclerView) view.findViewById(R.id.data_customer);
        linearLayout = (LinearLayout) view.findViewById(R.id.data_not_found);
        pencatatListAdapter = new PencatatListAdapter(surveyList, context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pencatatListAdapter);

        swap();

        TextView notFound = (TextView) view.findViewById(R.id.month_now);
        notFound.setText(String.format("Bulan %s", DateTimeUtil.format(DateTimeUtil.now(), Constants.DATE_MONTH_YEAR)));

        Button button = (Button) view.findViewById(R.id.button_input);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMeter();
            }
        });

        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(Constants.DEBUG) Log.d(TAG, "Month Selected  " + adapter.getItem(position));
                filterData(DateTimeUtil.format(DateTimeUtil.parse(adapter.getItem(position), Constants.DATE_MONTH_YEAR), "MM"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void inputMeter(){
        NewInputMeterDialogFragment inputMeterDialogFragment = new NewInputMeterDialogFragment();
        inputMeterDialogFragment.setTargetFragment(this, DIALOG_FRAGMENT);
        inputMeterDialogFragment.show(getFragmentManager(),"ACTION BAR DIALOG");
    }

    public void refreshData(){
        surveyList = surveyTable.getAll();
        pencatatListAdapter.swapData(surveyList);
        swap();
    }

    private void filterData(String month){
        surveyList = surveyTable.getByMonth(month);
        pencatatListAdapter.swapData(surveyList);
        swap();
    }

    private void swap(){
        if(surveyList.size() > 0){
            linearLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else{
            linearLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case DIALOG_FRAGMENT:
                refreshData();
                break;
        }
    }
}
