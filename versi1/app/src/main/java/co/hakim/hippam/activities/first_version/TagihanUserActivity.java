/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;
import co.hakim.hippam.adapters.TagihanUserListAdapter;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class TagihanUserActivity extends AppCompatActivity implements DataLoadListener {

    private static final String TAG = TagihanUserActivity.class.getSimpleName();

    private int offset = 0;
    private TagihanUserListAdapter tagihanUserListAdapter;
    private List<Tagihan> tagihanList;
    private DotProgressBar progressBar;
    private ImageView notFound;
    private RecyclerView listTagihanRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tagihan_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.list_tagihan);

        setSupportActionBar(toolbar);

        tagihanList = new ArrayList<>();
        progressBar = (DotProgressBar) findViewById(R.id.progress_bar);
        notFound = (ImageView) findViewById(R.id.not_found);

        listTagihanRecyclerView = (RecyclerView) findViewById(R.id.tagihan_user);
        tagihanUserListAdapter = new TagihanUserListAdapter(this, tagihanList, this);
        listTagihanRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        listTagihanRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listTagihanRecyclerView.setAdapter(tagihanUserListAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.feedback);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFeed();
            }
        });


        loadData();
    }

    private void startFeed(){
        Intent intent = new Intent(this, FeedbackListActivity.class);
        startActivity(intent);
    }

    private void loadData(){
        if (offset < 0) {
            return;
        }

        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(tagihanList.size() > 0)
                        tagihanList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("tagihan");
                        int len = datum.length();

                        if (len == Constants.DATA_FETCH_LENGTH) {
                            offset += len;
                        } else {
                            offset = -1;
                        }

                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Tagihan tagihan = new Tagihan();
                            tagihan.setIdTag(data.getString("id_tag"));
                            tagihan.setTagihanBulan(data.getString("tagihan"));
                            tagihan.setJumlahTagihan(data.getInt("jumlah"));
                            tagihan.setStatusTagihan(data.getString("status"));

                            tagihanList.add(tagihan);
                        }

                        tagihanUserListAdapter.swapData(tagihanList);
                        listTagihanRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStart() {
                super.onStart();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressBar.setVisibility(View.GONE);

                if (tagihanUserListAdapter.getItemCount() > 0 &&
                        tagihanUserListAdapter.getItemViewType(tagihanUserListAdapter.getItemCount() - 1) ==
                                TagihanUserListAdapter.VIEW_PROGRESS) {
                    tagihanUserListAdapter.remove(tagihanUserListAdapter.getItemCount() - 1);
                }
            }
        };

        RestClient.getInstance(this, tagihanResponseHandler).getTagihanUser(UserUtil.getInstance(this).getToken(), offset , true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            UserUtil.getInstance(this).signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDataFetchStarted() {

    }

    @Override
    public void onDataFetchFinished() {

    }

    @Override
    public void onLastData() {
        loadData();
    }
}
