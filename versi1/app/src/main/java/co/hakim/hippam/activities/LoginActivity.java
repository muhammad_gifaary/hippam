package co.hakim.hippam.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.activities.first_version.PencatatMeterActivity;
import co.hakim.hippam.activities.second_version.NewLoketActivity;
import co.hakim.hippam.activities.second_version.UserActivity;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.providers.HippamContentProviders;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private TextInputLayout mUsernameField;
    private TextInputLayout mPasswordField;
    private TextView errorLogin;
    private DotProgressBar progressBar;
    private JsonHttpResponseHandler loginHandler;

    //Table Init
    private CustomerTable customerTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.action_sign_in);

        setSupportActionBar(toolbar);

        setupActionBar();

        if(UserUtil.getInstance(this).isLoggedIn()) {
            if(UserUtil.getInstance(this).getUserStatus() == Constants.USER_TYPE_ENUMERATOR_A || UserUtil.getInstance(this).getUserStatus() == Constants.USER_TYPE_ENUMERATOR_B){
                enums();
            }else{
                startMenu(UserUtil.getInstance(this).getUserStatus());
            }
        }

        progressBar = (DotProgressBar) findViewById(R.id.progress_bar);

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);

        mUsernameField = (TextInputLayout) findViewById(R.id.username_field);
        mPasswordField = (TextInputLayout) findViewById(R.id.password_field);

        errorLogin = (TextView) findViewById(R.id.error_login);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            //noinspection ConstantConditions
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.login);
        }
    }

    public void attemptLogin(){
        if (loginHandler != null) {
            return;
        }

        // Reset errors.
        mUsernameField.setError(null);
        mPasswordField.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            if (!mPasswordField.isErrorEnabled()) mPasswordField.setErrorEnabled(true);
            mPasswordField.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            if (!mUsernameField.isErrorEnabled()) mUsernameField.setErrorEnabled(true);
            mUsernameField.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Kick off a background task to perform the user login attempt.
            loginHandler = new RestResponseHandler(TAG) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    if (Constants.DEBUG) Log.d(TAG, "login response: " + response);

                    try {
                        if (isSuccess(response)) {
                            JSONArray data = response.getJSONArray("data");
                            JSONArray pam = response.getJSONArray("pam");
                            finish(true, data.getJSONObject(0), pam.getJSONObject(0));
                        } else {
                            progressBar.setVisibility(View.GONE);
                            mUsernameView.setEnabled(true);
                            mPasswordView.setEnabled(true);

                            new CountDownTimer(5000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    errorLogin.setVisibility(View.VISIBLE);
                                }

                                public void onFinish() {
                                    errorLogin.setVisibility(View.GONE);
                                }
                            }.start();

                            loginHandler = null;
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "login response: " + response, e);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d(TAG, "response is string: " + responseString);
                }

                @Override
                public void onStart() {
                    super.onStart();
                    progressBar.setVisibility(View.VISIBLE);
                    mUsernameView.setEnabled(false);
                    mPasswordView.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    super.onFinish();

                }

                @Override
                public void onCancel() {
                    super.onCancel();
                    progressBar.setVisibility(View.GONE);
                    loginHandler = null;
                }
            };

            String token = FirebaseInstanceId.getInstance().getToken();

            RestClient.getInstance(this, loginHandler).postLogin(username, password, token, true);
        }
    }

    private void finishLoad(){
        loginHandler = null;
        progressBar.setVisibility(View.GONE);
        mUsernameView.setEnabled(true);
        mPasswordView.setEnabled(true);
    }

    private void finish(boolean ok, JSONObject data, JSONObject pam) {
        if (ok) {
            int status = 0;
            try {
                UserUtil.getInstance(this).signIn(data, pam);
                status = data.getInt("status");
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage(), e);
            }

            Toast.makeText(this, R.string.success_login, Toast.LENGTH_SHORT).show();
            startMenu(status);
        }
    }

    private void startMenu(int status){
        Intent intent;
        switch (status){
            case Constants.USER_TYPE_ENUMERATOR_A:
            case Constants.USER_TYPE_ENUMERATOR_B:
                startEnumerator(status);
                break;
            case Constants.USER_TYPE_LOKET:
                if(!UserUtil.getInstance(this).isLoggedIn()){
                    finishLoad();
                }

                getSaldo();
                intent = new Intent(this, NewLoketActivity.class);
                startActivity(intent);
                finish();
                break;
            case Constants.USER_TYPE_CUSTOMER:
                if(!UserUtil.getInstance(this).isLoggedIn()){
                    finishLoad();
                }

                intent = new Intent(this, UserActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    private void setSaldo(String saldo){
        UserUtil.getInstance(this).setSaldo(saldo);
    }

    private void getSaldo(){
        JsonHttpResponseHandler saldoResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        setSaldo(response.getString("saldo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(this, saldoResponseHandler).getSaldo(UserUtil.getInstance(this).getToken(), true);
    }

    private void startEnumerator(int status){

        customerTable = new CustomerTable(this);
        getContentResolver().delete(HippamContentProviders.URI_CUSTOMERS, null, null);

        JsonHttpResponseHandler customerResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray data = response.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject datum = data.getJSONObject(i);
                            Customer c = new Customer();
                            CustomerTable.setValues(datum, c);
                            customerTable.sync(c);
                        }

                        enums();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                enums();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                finishLoad();
            }
        };

        RestClient.getInstance(this, customerResponseHandler).getCustomer(UserUtil.getInstance(this).getToken(), true);
    }

    private void enums(){
        Intent intent = new Intent(this, PencatatMeterActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
