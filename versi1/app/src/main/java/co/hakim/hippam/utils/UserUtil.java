package co.hakim.hippam.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;

/**
 * Created by Hakim on 27-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class UserUtil {
    private static UserUtil instance;

    private SharedPreferences preferences;

    private UserUtil() {

    }

    public static UserUtil getInstance(Context context) {
        if (instance == null) {
            instance = new UserUtil();
            instance.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        return instance;
    }

    public String getStringProperty(String key) {
        return preferences.getString(key, "");
    }

    public void setStringProperty(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value).apply();
    }

    public boolean getBooleanProperty(String key) {
        return preferences.getBoolean(key, false);
    }

    public void setBooleanProperty(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value).apply();
    }

    public int getIntProperty(String key) {
        return preferences.getInt(key, 0);
    }

    public void setIntProperty(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value).apply();
    }

    public float getFloatProperty(String key) {
        return preferences.getFloat(key, 0.0F);
    }

    public void setFloatProperty(String key, float value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value).apply();
    }

    /**
     * Clearing all saved preferences, used for logging out
     */
    public void reset() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
    }

    public boolean isLoggedIn() {
        return !getToken().isEmpty();
    }

    public void signIn(JSONObject data, JSONObject pam) throws JSONException {
        setStringProperty(Constants.USER_TOKEN, data.getString(Constants.JSON_USER_TOKEN));
        setStringProperty(Constants.USER_EMAIL, data.getString(Constants.JSON_EMAIL));
        setStringProperty(Constants.USER_FULL_NAME, data.getString(Constants.JSON_USER_FULL_NAME));
        setIntProperty(Constants.USER_STATUS, data.getInt(Constants.JSON_USER_STATUS));
        setStringProperty(Constants.USER_INST, pam.getString("nama"));
        setStringProperty(Constants.USER_ADDRESS, data.getString(Constants.JSON_USER_ADDRESS));
    }

    public void signOut() {
        reset();
    }

    public String getToken() {
        return getStringProperty(Constants.USER_TOKEN);
    }

    public String getEmail() {
        return getStringProperty(Constants.USER_EMAIL);
    }

    public String getFullName() {
        return getStringProperty(Constants.USER_FULL_NAME);
    }

    public String getAddress() {
        return getStringProperty(Constants.USER_ADDRESS);
    }

    public int getUserStatus() {
        return getIntProperty(Constants.USER_STATUS);
    }

    public String getSaldo() { return  getStringProperty(Constants.USER_SALDO); }

    public void setSaldo(String saldo) { setStringProperty(Constants.USER_SALDO, saldo); }

    public String getInst() { return getStringProperty(Constants.USER_INST); }
}
