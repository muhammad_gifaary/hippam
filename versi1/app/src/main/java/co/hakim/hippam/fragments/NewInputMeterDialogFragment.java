/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * Created by hakim on 11/4/16.
 */

public class NewInputMeterDialogFragment extends DialogFragment {

    private static final String TAG = NewInputMeterDialogFragment.class.getSimpleName();

    private Context context;
    private CustomerTable customerTable;
    private SurveyTable surveyTable;
    private Toolbar toolbar;
    private LinearLayout containerInputId, containerInputMeter;
    private EditText inputCustId, inputMeter;
    private TextInputLayout inputCustIdField;
    private TextView name, address, id, group, meterLalu;
    private RecyclerView pembayaranRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(getActivity());

        customerTable = new CustomerTable(context);
        surveyTable = new SurveyTable(context);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.input_customer_id_dialog, null);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.input_meter);

        containerInputId = (LinearLayout) view.findViewById(R.id.container_input_id);
        containerInputMeter = (LinearLayout) view.findViewById(R.id.container_input_meter);

        inputCustId = (EditText) view.findViewById(R.id.input_cust_id);
        inputMeter = (EditText) view.findViewById(R.id.meter_now);
        inputCustIdField = (TextInputLayout) view.findViewById(R.id.inputmeter_field);

        name = (TextView) view.findViewById(R.id.customer_name);
        address = (TextView) view.findViewById(R.id.customer_address);
        id = (TextView) view.findViewById(R.id.customer_id);
        group = (TextView) view.findViewById(R.id.customer_group);
        meterLalu = (TextView) view.findViewById(R.id.meter_ago);

        Button saveSurvey = (Button) view.findViewById(R.id.save_meter);
        saveSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        Button nextId = (Button) view.findViewById(R.id.btn_next);
        nextId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transition();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void setData(Customer customer){
        Survey survey = surveyTable.getByIdAndDate(customer.getCustomerId(), DateTimeUtil.monthAgo(1));
        if(survey != null){
            meterLalu.setText(String.valueOf(survey.getInputMeter()));
        }else{
            meterLalu.setText("0");
        }

        name.setText(customer.getCustomerName());
        address.setText(customer.getCustomerAddress());
        id.setText(customer.getCustomerId());
        group.setText(customer.getCustomerGroup());
    }

    private void saveData(){
        if(TextUtils.isEmpty(inputMeter.getText().toString())){
            inputMeter.requestFocus();
        }else{
            Survey survey = new Survey();
            survey.setCustomerId(id.getText().toString());
            survey.setInputDate(DateTimeUtil.now());
            int meter = Integer.parseInt(inputMeter.getText().toString());
            survey.setInputMeter(meter);
            survey.setSyncDate(DateTimeUtil.formatDBDateOnly((DateTimeUtil.now())));
            survey.setSyncStatus(Constants.SYNC_STATUS_PENDING);

            surveyTable.insert(survey);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
            dismiss();
        }
    }

    private void transition(){
        if(TextUtils.isEmpty(inputCustId.getText().toString())){
            if (!inputCustIdField.isErrorEnabled()) inputCustIdField.setErrorEnabled(true);
            inputCustIdField.setError(getString(R.string.error_field_required));
            inputCustId.requestFocus();
        }else{
            Customer customer = customerTable.getById(inputCustId.getText().toString());

            if(customer != null){
                Survey survey = surveyTable.getByIdAndDate(customer.getCustomerId(), DateTimeUtil.now());

                if(survey != null){
                    if (!inputCustIdField.isErrorEnabled()) inputCustIdField.setErrorEnabled(true);
                    inputCustIdField.setError(getString(R.string.survey_found));
                    inputCustId.requestFocus();
                }else{
                    setData(customer);
                    toolbar.setTitle(R.string.customer_detail);

                    inputMeter.requestFocus();
                    containerInputId.setVisibility(View.GONE);
                    containerInputMeter.setVisibility(View.VISIBLE);
                }
            }else{
                if (!inputCustIdField.isErrorEnabled()) inputCustIdField.setErrorEnabled(true);
                inputCustIdField.setError(getString(R.string.user_not_found));
                inputCustId.requestFocus();
            }
        }
    }
}
