/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * Created by hakim on 11/7/16.
 */

public class InputComplain extends DialogFragment {
    private static final String TAG = InputComplain.class.getSimpleName();

    private Context context;
    private TextView report;
    private TextInputLayout reportField;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.input_complain, null);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Kirim Pengaduan");

        report = (TextView) view.findViewById(R.id.report);
        reportField = (TextInputLayout) view.findViewById(R.id.report_field);
        Button button = (Button) view.findViewById(R.id.send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushData(report.getText().toString());
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void pushData(final String reports){

        if(TextUtils.isEmpty(reports)){
            reportField.setError(getString(R.string.error_field_required));
            report.requestFocus();
        }else{
            JsonHttpResponseHandler reportResponseHandler = new JsonHttpResponseHandler(){
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    try {
                        if (Constants.DEBUG) {
                            Log.d(TAG, "response = " + response.toString(4));
                        }

                        if(RestResponseHandler.isSuccess(response)){
                            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                            getDialog().dismiss();
                        }else{
                            reportField.setError(response.getString("msg"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    report.setEnabled(true);
                }

                @Override
                public void onStart() {
                    super.onStart();
                    report.setEnabled(false);
                }
            };

            RestClient.getInstance(context, reportResponseHandler).postReport(UserUtil.getInstance(context).getToken(),reports, true);
        }
    }
}
