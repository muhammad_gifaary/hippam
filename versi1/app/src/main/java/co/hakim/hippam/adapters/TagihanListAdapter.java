/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class TagihanListAdapter extends RecyclerView.Adapter<TagihanListAdapter.TagihanListViewHolder>{

    private static final String TAG = TagihanListAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private ItemClickListener itemClickListener;

    private Context context;

    public TagihanListAdapter(List<Tagihan> tagihanList, ItemClickListener itemClickListener, Context context) {
        this.tagihanList = tagihanList;
        this.itemClickListener = itemClickListener;
        this.context = context;
    }

    @Override
    public TagihanListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TagihanListViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_tagihan, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(TagihanListViewHolder holder, int position) {
        Tagihan tagihan = tagihanList.get(position);
        holder.bulanTagihan.setText(tagihan.getTagihanBulan());
        holder.jumlahBayar.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));
        holder.namaPelanggan.setText(tagihan.getNamaPel());

        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(TagihanListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(tagihanList != null){
            return tagihanList.size();
        }

        return 0;
    }

    public void swapData(List<Tagihan> tagihanList){
        if(tagihanList != null){
            this.tagihanList = tagihanList;
        }

        notifyDataSetChanged();
    }

    public Tagihan getTagihan(int position){
        return tagihanList.get(position);
    }


    public class TagihanListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView bulanTagihan, jumlahBayar, namaPelanggan;
        private Button bayarTagihan;
        public TagihanListViewHolder(final View itemView, final ItemClickListener itemClickListener) {
            super(itemView);
            container = (View) itemView.findViewById(R.id.container);
            bulanTagihan = (TextView) itemView.findViewById(R.id.bulan_tagihan);
            jumlahBayar = (TextView) itemView.findViewById(R.id.tagihan);
            namaPelanggan = (TextView) itemView.findViewById(R.id.customer_name);
            bayarTagihan = (Button) itemView.findViewById(R.id.pay_tagihan);
            bayarTagihan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        itemClickListener.onItemClicked(getAdapterPosition(), itemView);
                    }
                }
            });

        }
    }
}
