/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.utils;

import android.util.Log;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import co.hakim.hippam.Constants;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class Formatter {
    public static final String LOCATION_FORMAT = "%f,%f";
    private static final DecimalFormat DF = new DecimalFormat();

    //Format from 1000000.2 to 1.000.000,20
    public static String toCurrency(Double d) {
        if (d == null || "".equals(d) || "NaN".equals(d)) {
            return " - ";
        }
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        DecimalFormatSymbols symbols = DF.getDecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        String ret = DF.format(bd) + "";
        if (ret.indexOf(",") == -1) {
            ret += ",00";
        }
        if (ret.split(",")[1].length() != 2) {
            ret += "0";
        }
        return ret;
    }

    public static String nvl(TextView view) {
        if (view.getText().toString().equals("null")) {
            return "-";
        } else {
            return view.getText().toString();
        }
    }

    public static String formatRupiah(String nominal) {
        String rupiah = "";
        DecimalFormat df;
        df = new DecimalFormat("#,###.##");
        df.setDecimalSeparatorAlwaysShown(true);
        String v =
                nominal.replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()),
                        "");
        try {
            Number n = df.parse(v);
            rupiah = df.format(n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rupiah;
    }

    public static String formatRupiahNoDecimal(String nominal) {
        String rupiah = "";
        DecimalFormat df;
        df = new DecimalFormat("#,###");
        df.setDecimalSeparatorAlwaysShown(false);
        String v =
                nominal.replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()),
                        "");
        try {
            Number n = df.parse(v);
            rupiah = df.format(n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rupiah;
    }

    public static String formatRupiah(double nominal) {
        String rupiah = "";
        DecimalFormat df;
        df = new DecimalFormat("#,###.##");
        df.setDecimalSeparatorAlwaysShown(true);
        String nilai = String.format("%.0f", nominal);
        Log.d("Format", nilai);
        String v =
                nilai.replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()),
                        "");
        try {
            Number n = df.parse(v);
            rupiah = df.format(n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rupiah;
    }

    public static String formatRupiahLong(double nominal) {
        String rupiah = "";
        DecimalFormat df;
        df = new DecimalFormat("#,###,###,###");
        df.setDecimalSeparatorAlwaysShown(true);
        String nilai = String.format("%.0f", nominal);
        Log.d("Format", nilai);
        String v =
                nilai.replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()),
                        "");
        try {
            Number n = df.parse(v);
            rupiah = df.format(n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rupiah;
    }

    public static String formatDouble(double value) {
        //apa ya
        String tmp = String.valueOf(value).replace(",", ".");
        value = Double.parseDouble(tmp);
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value);
    }

    public static double doubleValue(String value) {

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        try {
            return nf.parse(value).doubleValue();
        } catch (ParseException e) {
        }

        nf = NumberFormat.getNumberInstance(Locale.getDefault());
        try {
            return nf.parse(value).doubleValue();
        } catch (ParseException e) {
        }

        return 0;
    }

    public static String doubleValue(double value) {
        String result = "0";

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        try {
            return nf.format(value);
        } catch (Exception e) {
        }

        nf = NumberFormat.getNumberInstance(Locale.getDefault());
        try {
            return nf.format(value);
        } catch (Exception e) {
        }


        return "0";
    }

    public static String roundingDoubleValue(double value) {
        Locale indonesia = new Locale("in", "ID");
        NumberFormat nf = NumberFormat.getNumberInstance(indonesia);
        try {
            BigDecimal decimal = new BigDecimal(String.valueOf(value)).setScale(0, RoundingMode.UP);
            return nf.format(decimal);
        } catch (Exception e) {
        }

        return "0";
    }

    public static String roundingDoubleValue2(double value) {
        Locale indonesia = new Locale("in", "ID");
        NumberFormat nf = NumberFormat.getNumberInstance(indonesia);
        try {
            BigDecimal decimal = new BigDecimal(String.valueOf(value)).setScale(2, RoundingMode.UP);
            return nf.format(decimal);
        } catch (Exception e) {
        }

        return "0";
    }

    public static String roundingDoubleValueHalf(double value) {
        Locale indonesia = new Locale("in", "ID");
        NumberFormat nf = NumberFormat.getNumberInstance(indonesia);
        try {
            BigDecimal decimal =
                    new BigDecimal(String.valueOf(value)).setScale(2, BigDecimal.ROUND_HALF_UP);
            return nf.format(decimal);
        } catch (Exception e) {
        }

        return "0";
    }

    public static String longlat(double longitude, double latitude) {
        return String.format(Locale.ENGLISH, LOCATION_FORMAT, longitude, latitude);
    }

    public static String format(long value, Locale locale) {
        if (Constants.DEBUG) Log.d("Formatter", "foo");
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        return formatter.format(value);
    }

    public static String format(double value, Locale locale) {
        if (Constants.DEBUG) Log.d("Formatter", "foo");
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        return formatter.format(value);
    }
}
