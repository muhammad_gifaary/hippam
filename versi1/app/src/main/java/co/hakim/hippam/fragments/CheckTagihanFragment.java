/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.activities.first_version.DetailTagihanActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckTagihanFragment extends Fragment {


    public CheckTagihanFragment() {
        // Required empty public constructor
    }

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_tagihan, container, false);
        final TextInputLayout noPelField = (TextInputLayout) view.findViewById(R.id.input_nopel_field);
        final EditText noPel = (EditText) view.findViewById(R.id.input_nopel);
        Button checkTag = (Button) view.findViewById(R.id.check_tagihan);
        noPelField.setError(null);

        checkTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(noPel.getText().toString())){
                    noPelField.setError(getString(R.string.error_field_required));
                    noPel.requestFocus();
                }else{
                    Intent intent = new Intent(context, DetailTagihanActivity.class);
                    intent.putExtra(Constants.EXTRA_NOPEL, noPel.getText().toString());
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
