/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.ReportListAdapter;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class PengaduanFragment extends Fragment implements DataLoadListener{

    private static final String TAG = PengaduanFragment.class.getSimpleName();

    public static final int DIALOG_FRAGMENT = 1;
    private int offset = 0;
    private List<Report> reportList;
    private ImageView notFound;
    private RecyclerView listReportRecyclerView;
    private ReportListAdapter reportListAdapter;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public PengaduanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pengaduan, container, false);
        notFound = (ImageView) view.findViewById(R.id.not_found);
        listReportRecyclerView = (RecyclerView) view.findViewById(R.id.list_pengaduan);
        reportListAdapter = new ReportListAdapter(null, context, this);
        listReportRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        listReportRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listReportRecyclerView.setAdapter(reportListAdapter);

        LinearLayout sentComplain = (LinearLayout) view.findViewById(R.id.send_complain);
        sentComplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComplain();
            }
        });

        return view;
    }

    private void sendComplain(){
        InputComplain inputComplain = new InputComplain();
        inputComplain.setTargetFragment(this, DIALOG_FRAGMENT);
        inputComplain.show(getFragmentManager(), "Input Complain");
    }

    @Override
    public void onResume() {
        super.onResume();
        offset = 0;
        loadData();
    }

    private void loadData(){
        if (offset < 0) {
            return;
        }

        JsonHttpResponseHandler reportResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");

                        if(offset == 0){
                            if (reportListAdapter.getItemCount() > 0){
                                reportListAdapter.clearItems();
                            }
                        }

                        int len = datum.length();

                        if (len == Constants.DATA_FETCH_LENGTH) {
                            offset += len;
                        } else {
                            offset = -1;
                        }

                        for (int i=0; i < len; i++){
                            JSONObject data = datum.getJSONObject(i);
                            Report report = new Report();
                            report.setId(data.getInt("id"));
                            report.setProgress(data.getString("progres"));
                            report.setReport(data.getString("uraian_aduan"));

//                            reportList.add(report);
                            reportListAdapter.add(report);
                        }

//                        reportListAdapter.swapData(reportList);
                        if(reportListAdapter.getItemCount() > 0){
                            listReportRecyclerView.setVisibility(View.VISIBLE);
                        }
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (reportListAdapter.getItemCount() > 0 &&
                        reportListAdapter.getItemViewType(reportListAdapter.getItemCount() - 1) ==
                                reportListAdapter.VIEW_PROGRESS) {
                    reportListAdapter.remove(reportListAdapter.getItemCount() - 1);
                }
            }
        };

        RestClient.getInstance(context, reportResponseHandler).getListReport(UserUtil.getInstance(context).getToken(), offset, true);
    }

    @Override
    public void onDataFetchStarted() {

    }

    @Override
    public void onDataFetchFinished() {

    }

    @Override
    public void onLastData() {
        loadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case DIALOG_FRAGMENT:
                onResume();
                break;
        }
    }
}
