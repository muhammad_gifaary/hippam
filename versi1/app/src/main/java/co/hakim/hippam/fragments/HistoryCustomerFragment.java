package co.hakim.hippam.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.SurveyListAdapter;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.fragments.first_version.InputMeterDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryCustomerFragment extends Fragment {

    private static final String TAG = HistoryCustomerFragment.class.getSimpleName();
    public static final int DIALOG_FRAGMENT = 1;

    public static HistoryCustomerFragment newInstance(Customer customer) {
        HistoryCustomerFragment fragment = new HistoryCustomerFragment();

        Bundle args = new Bundle();
        args.putParcelable(Constants.EXTRA_CUSTOMER, customer);

        fragment.setArguments(args);
        return fragment;
    }

    private Context context;
    private Customer customer;
    private SurveyTable surveyTable;
    private SurveyListAdapter surveyListAdapter;
    private List<Survey> surveyList;
    private FloatingActionButton inputMeterButton;

    public HistoryCustomerFragment() {
        // Required empty public constructor
    }

    private BroadcastReceiver syncReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.DEBUG) Log.i(TAG, "sync progress update");
            int progress = intent.getIntExtra(
                    Constants.EXTRA_SYNC_PROGRESS, Constants.SYNC_PROGRESS_FINISHED
            );

            if (progress == Constants.SYNC_PROGRESS_FINISHED) {
                onUpdate();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            customer = savedInstanceState.getParcelable(Constants.EXTRA_CUSTOMER);
        }

        if (getArguments() != null) {
            customer = getArguments().getParcelable(Constants.EXTRA_CUSTOMER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_customer, container, false);
        surveyTable = new SurveyTable(context);
        surveyList = surveyTable.getByCustomerId(customer.getCustomerId());
        surveyListAdapter = new SurveyListAdapter(surveyList, context);
        RecyclerView surveyLisRecyclerView = (RecyclerView) view.findViewById(R.id.list_history);
        surveyLisRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        surveyLisRecyclerView.setItemAnimator(new DefaultItemAnimator());
        surveyLisRecyclerView.setAdapter(surveyListAdapter);

        inputMeterButton = (FloatingActionButton) view.findViewById(R.id.add_meter);
        inputMeterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInputMeter();
            }
        });

        checkSurvey();

        return view;
    }

    private void checkSurvey(){
        Survey survey = surveyTable.getById(customer.getCustomerId());
        if(survey != null){
            inputMeterButton.setVisibility(View.GONE);
        }
    }

    private void openInputMeter(){
        InputMeterDialogFragment inputMeterDialogFragment = InputMeterDialogFragment.newInstance(customer);
        inputMeterDialogFragment.setTargetFragment(this, DIALOG_FRAGMENT);
        inputMeterDialogFragment.show(getFragmentManager(),"ACTION BAR DIALOG");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void onUpdate(){
        surveyList = surveyTable.getByCustomerId(customer.getCustomerId());
        surveyListAdapter.swapData(surveyList);
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(
                syncReceiver, new IntentFilter(Constants.INTENT_ACTION_SYNC));
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(syncReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case DIALOG_FRAGMENT:
                onUpdate();
                checkSurvey();
                break;
        }
    }
}
