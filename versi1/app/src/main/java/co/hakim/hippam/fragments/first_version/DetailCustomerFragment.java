/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments.first_version;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Customer;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailCustomerFragment extends Fragment {

    private Customer customer;

    public static DetailCustomerFragment newInstance(Customer customer) {
        DetailCustomerFragment fragment = new DetailCustomerFragment();

        Bundle args = new Bundle();
        args.putParcelable(Constants.EXTRA_CUSTOMER, customer);

        fragment.setArguments(args);
        return fragment;
    }

    public DetailCustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            customer = savedInstanceState.getParcelable(Constants.EXTRA_CUSTOMER);
        }

        if (getArguments() != null) {
            customer = getArguments().getParcelable(Constants.EXTRA_CUSTOMER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_customer, container, false);

        TextView customerNumber = (TextView) view.findViewById(R.id.customer_number);
        TextView customerName = (TextView) view.findViewById(R.id.customer_name);
        TextView customerAddress = (TextView) view.findViewById(R.id.customer_address);
        TextView customerTelp = (TextView) view.findViewById(R.id.customer_telp);
        TextView customerMeter = (TextView) view.findViewById(R.id.customer_meter);
        TextView customerGroup = (TextView) view.findViewById(R.id.customer_group);
        TextView customerArea = (TextView) view.findViewById(R.id.customer_area);

        customerNumber.setText(customer.getCustomerId());
        customerName.setText(customer.getCustomerName());
        customerAddress.setText(customer.getCustomerAddress());
        customerTelp.setText(customer.getCustomerPhone());
        customerMeter.setText(customer.getMeterId());
        customerGroup.setText(customer.getCustomerGroup());
        customerArea.setText(customer.getCustomerArea());

        return view;
    }

}
