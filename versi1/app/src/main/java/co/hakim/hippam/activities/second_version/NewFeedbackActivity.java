/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.second_version;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class NewFeedbackActivity extends AppCompatActivity {

    private static final String TAG = NewFeedbackActivity.class.getSimpleName();

    private DotProgressBar progressBar;
    private TextView report;
    private TextInputLayout reportField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_feedback);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.send_report);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        report = (TextView) findViewById(R.id.report);
        progressBar = (DotProgressBar) findViewById(R.id.progress_bar);
        reportField = (TextInputLayout) findViewById(R.id.report_field);
        Button button = (Button) findViewById(R.id.send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushData(report.getText().toString());
            }
        });
    }

    private void pushData(final String reports){

        if(TextUtils.isEmpty(reports)){
            reportField.setError(getString(R.string.error_field_required));
            report.requestFocus();
        }else{
            JsonHttpResponseHandler reportResponseHandler = new JsonHttpResponseHandler(){
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    try {
                        if (Constants.DEBUG) {
                            Log.d(TAG, "response = " + response.toString(4));
                        }

                        if(RestResponseHandler.isSuccess(response)){
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progressBar.setVisibility(View.GONE);
                    report.setEnabled(true);
                }

                @Override
                public void onStart() {
                    super.onStart();
                    progressBar.setVisibility(View.VISIBLE);
                    report.setEnabled(false);
                }
            };

            RestClient.getInstance(this, reportResponseHandler).postReport(UserUtil.getInstance(this).getToken(),reports, true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home :
                onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
