/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.HistoryPembayaranAdapter;
import co.hakim.hippam.db.tables.TagihanTable;
import co.hakim.hippam.domains.models.Rekening;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewHistoryPembayaranFragment extends Fragment {

    private static final String TAG = NewHistoryPembayaranFragment.class.getSimpleName();

    private Context context;
    private HistoryPembayaranAdapter historyPembayaranAdapter;
    private TagihanTable tagihanTable;
    private LinearLayout not_found;
    private RecyclerView recyclerView;
    private ArrayList<Rekening> rekeningArrayList;

    public NewHistoryPembayaranFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_history_pembayaran, container, false);

        tagihanTable = new TagihanTable(context);
        rekeningArrayList = new ArrayList<>();
        loadRek();

        historyPembayaranAdapter = new HistoryPembayaranAdapter(tagihanTable.getAll(), context);

        TextView saldo = (TextView) view.findViewById(R.id.sisa_saldo);

        saldo.setText(String.format("Rp %s", UserUtil.getInstance(context).getSaldo()));

        recyclerView = (RecyclerView) view.findViewById(R.id.history_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyPembayaranAdapter);

        not_found = (LinearLayout) view.findViewById(R.id.not_found);
        refreshData();

        LinearLayout topup = (LinearLayout) view.findViewById(R.id.top_up_saldo);
        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topup();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void topup(){
        TopUpSaldoDialog topUpSaldoDialog = TopUpSaldoDialog.newInstance(rekeningArrayList);
        topUpSaldoDialog.show(getFragmentManager(), "Show Saldo");
    }

    private void refreshData(){
        historyPembayaranAdapter.swapData(tagihanTable.getAll());
        if(tagihanTable.getAll().size() > 0){
            not_found.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else{
            not_found.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            refreshData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    private void loadRek(){
        JsonHttpResponseHandler reportResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(rekeningArrayList.size() > 0)
                        rekeningArrayList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");

                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Rekening rekening = new Rekening();
                            rekening.setId(data.getInt("id"));
                            rekening.setNoRekening(data.getString("bank"));

                            rekeningArrayList.add(rekening);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, reportResponseHandler).getRekening(UserUtil.getInstance(context).getToken(), true);
    }
}
