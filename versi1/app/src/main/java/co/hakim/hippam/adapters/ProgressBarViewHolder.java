/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import co.hakim.hippam.R;

/**
 * Created by Hakim on 12-Nov-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */

public class ProgressBarViewHolder extends RecyclerView.ViewHolder {
    private ProgressBar progressBar;
    private View container;

    public ProgressBarViewHolder(View itemView) {
        super(itemView);

        container = itemView.findViewById(R.id.container);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
    }

    public View getContainer() {
        return container;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
