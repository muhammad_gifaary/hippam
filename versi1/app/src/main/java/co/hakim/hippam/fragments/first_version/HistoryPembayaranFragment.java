/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments.first_version;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.hakim.hippam.R;
import co.hakim.hippam.adapters.HistoryTagihanAdapter;
import co.hakim.hippam.db.tables.TagihanTable;
import co.hakim.hippam.domains.models.Tagihan;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryPembayaranFragment extends Fragment {


    public HistoryPembayaranFragment() {
        // Required empty public constructor
    }

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_pembayaran, container, false);
        TagihanTable tagihanTable = new TagihanTable(context);
        List<Tagihan> tagihanList = tagihanTable.getAll();
        RecyclerView tagihanRecyclerView = (RecyclerView) view.findViewById(R.id.history_pembayaran);
        HistoryTagihanAdapter historyTagihanAdapter = new HistoryTagihanAdapter(context, tagihanList);
        tagihanRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        tagihanRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tagihanRecyclerView.setAdapter(historyTagihanAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
