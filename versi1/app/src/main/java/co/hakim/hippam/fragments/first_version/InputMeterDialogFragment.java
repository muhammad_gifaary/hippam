/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments.first_version;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class InputMeterDialogFragment extends DialogFragment {
    private static final String TAG = InputMeterDialogFragment.class.getSimpleName();

    public static InputMeterDialogFragment newInstance(Customer customer) {
        InputMeterDialogFragment fragment = new InputMeterDialogFragment();

        Bundle args = new Bundle();
        args.putParcelable(Constants.EXTRA_CUSTOMER, customer);

        fragment.setArguments(args);
        return fragment;
    }

    private Customer customer;

    private EditText inputMeter;
    private SurveyTable surveyTable;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            customer = savedInstanceState.getParcelable(Constants.EXTRA_CUSTOMER);
        }

        if (getArguments() != null) {
            customer = getArguments().getParcelable(Constants.EXTRA_CUSTOMER);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(getActivity())
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(TextUtils.isEmpty(inputMeter.getText().toString())){
                                    inputMeter.requestFocus();
                                }else{
                                    saveData();
                                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                                    dialog.dismiss();
                                }
                            }
                        }
                )
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
                                dialog.dismiss();
                            }
                        }
                );


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.input_meter, null);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.input_meter);

        TextView customerName = (TextView) view.findViewById(R.id.customer_name);
        TextView customerNumber = (TextView) view.findViewById(R.id.customer_id);
        TextView dateMonth = (TextView) view.findViewById(R.id.date_month);
        TextView dateYear = (TextView) view.findViewById(R.id.date_year);
        inputMeter = (EditText) view.findViewById(R.id.input_meter);

        customerName.setText(customer.getCustomerName());
        customerNumber.setText(customer.getCustomerId());
        dateMonth.setText(DateTimeUtil.format(DateTimeUtil.now(), Constants.DATE_MONTH));
        dateYear.setText(DateTimeUtil.format(DateTimeUtil.now(), Constants.DATE_YEAR));

        surveyTable = new SurveyTable(context);

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void saveData(){
        Survey survey = new Survey();
        survey.setCustomerId(customer.getCustomerId());
        survey.setInputMeter(Integer.parseInt(inputMeter.getText().toString()));
        survey.setInputDate(DateTimeUtil.now());
        survey.setSyncDate(DateTimeUtil.formatDBDateOnly((DateTimeUtil.now())));
        survey.setSyncStatus(Constants.SYNC_STATUS_PENDING);

        surveyTable.insert(survey);
    }
}
