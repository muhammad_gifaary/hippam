/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Rekening;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.Formatter;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * Created by hakim on 11/6/16.
 */

public class TopUpSaldoDialog extends DialogFragment {

    private static final String TAG = TopUpSaldoDialog.class.getSimpleName();

    private Context context;
    private EditText jumlahTopUp, noRekening, namaRekening, rekeningBank;
    private ArrayList<Rekening> rekeningList;
    private LinearLayout topup;
    private RelativeLayout done;
    private TextView saldo;


    public static TopUpSaldoDialog newInstance(ArrayList<Rekening> rekeningList) {
        TopUpSaldoDialog fragment = new TopUpSaldoDialog();

        Bundle args = new Bundle();
        args.putParcelableArrayList(Constants.EXTRA_REKENING, rekeningList);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            rekeningList = savedInstanceState.getParcelableArrayList(Constants.EXTRA_REKENING);
        }

        if (getArguments() != null) {
            rekeningList = getArguments().getParcelableArrayList(Constants.EXTRA_REKENING);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.top_up_saldo, null);

        List<String> rekList = new ArrayList<>();
        for (Rekening r : rekeningList){
            rekList.add(r.getNoRekening());
        }

        final Spinner spinner = (Spinner) view.findViewById(R.id.rek_tujuan);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, rekList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        topup = (LinearLayout) view.findViewById(R.id.container_topup);
        done = (RelativeLayout) view.findViewById(R.id.container_done);
        saldo = (TextView) view.findViewById(R.id.subs);

        jumlahTopUp = (EditText) view.findViewById(R.id.jumlah);
        noRekening = (EditText) view.findViewById(R.id.no_pemilik_rek);
        namaRekening = (EditText) view.findViewById(R.id.nama_pemilik);
        rekeningBank = (EditText) view.findViewById(R.id.rekening_asal);

        Button next = (Button) view.findViewById(R.id.btn_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View focus = null;
                boolean cancel = false;
                int id = 0;

                if(TextUtils.isEmpty(jumlahTopUp.getText().toString())){
                    jumlahTopUp.setError(getString(R.string.error_field_required));
                    focus = jumlahTopUp;
                    cancel = true;
                }

                if(TextUtils.isEmpty(noRekening.getText().toString())){
                    noRekening.setError(getString(R.string.error_field_required));
                    focus = noRekening;
                    cancel = true;
                }

                if(TextUtils.isEmpty(namaRekening.getText().toString())){
                    namaRekening.setError(getString(R.string.error_field_required));
                    focus = namaRekening;
                    cancel = true;
                }

                if(TextUtils.isEmpty(rekeningBank.getText().toString())){
                    rekeningBank.setError(getString(R.string.error_field_required));
                    focus = rekeningBank;
                    cancel = true;
                }

                for (Rekening r : rekeningList){
                    if(spinner.getSelectedItem().equals(r.getNoRekening())){
                        id = r.getId();
                        break;
                    }
                }

                if(cancel){
                    focus.requestFocus();
                }else{
                    pushData(id);
                }


            }
        });

        Button close = (Button) view.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });


        builder.setView(view);
        return builder.create();
    }

    private void pushData(int selectedId){
        JsonHttpResponseHandler reportResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        saldo.setText(String.format("Telah melakukan TopUp Saldo Rp. %s, segera kami check", Formatter.doubleValue(Double.parseDouble(String.valueOf(jumlahTopUp.getText().toString()))).replace(',','.')));
                        topup.setVisibility(View.GONE);
                        done.setVisibility(View.VISIBLE);
                        getSaldo();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }

            @Override
            public void onFinish() {
                super.onFinish();
            }

            @Override
            public void onStart() {
                super.onStart();
            }
        };

        RequestParams params = new RequestParams();
        params.put("token_key", UserUtil.getInstance(context).getToken());
        params.put("id_bank", selectedId);
        params.put("jumlah_transfer", jumlahTopUp.getText().toString());
        params.put("rekening_pemilik", namaRekening.getText().toString());
        params.put("rekening_nomor", noRekening.getText().toString());
        params.put("rekening_bank", rekeningBank.getText().toString());

        RestClient.getInstance(context, reportResponseHandler).postSaldo(params, true);
    }

    private void setSaldo(String saldo){
        UserUtil.getInstance(context).setSaldo(saldo);
    }

    private void getSaldo(){
        JsonHttpResponseHandler saldoResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        setSaldo(response.getString("saldo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, saldoResponseHandler).getSaldo(UserUtil.getInstance(context).getToken(), true);
    }
}
