package co.hakim.hippam.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.db.tables.TagihanTable;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class DBHelper extends SQLiteOpenHelper{
    private static final int DB_VERSION = 07;
    private static final String DB_NAME = "hippam.db";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        CustomerTable.onCreate(database);
        SurveyTable.onCreate(database);
        TagihanTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.d(getClass().getName(), "upgrading from version " + oldVersion + " to " + newVersion);
        CustomerTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        SurveyTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        TagihanTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}
