/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.db.tables.TagihanTable;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.Formatter;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class PaymentActivity extends AppCompatActivity {

    private static final String TAG = PaymentActivity.class.getSimpleName();

    private Tagihan tagihan;
    private boolean saldoCukup = false;
    private TagihanTable tagihanTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.detail_tagihan);

        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tagihan = getIntent().getParcelableExtra(Constants.EXTRA_TAGIHAN);
        tagihanTable = new TagihanTable(this);

        TextView bulanTagihan = (TextView) findViewById(R.id.bulan_tagihan);
        TextView idPelanggan = (TextView) findViewById(R.id.customer_number);
        TextView namaPelanggan = (TextView) findViewById(R.id.customer_name);
        TextView alamatPelanggan = (TextView) findViewById(R.id.customer_address);
        TextView totalTagihan = (TextView) findViewById(R.id.jumlah_bayar);
        final TextView saldo = (TextView) findViewById(R.id.saldo);
        Button bayar = (Button) findViewById(R.id.payment);

        bulanTagihan.setText(tagihan.getTagihanBulan());
        idPelanggan.setText(tagihan.getNoPel());
        namaPelanggan.setText(tagihan.getNamaPel());
        alamatPelanggan.setText(tagihan.getAlamatPel());
        totalTagihan.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));
        saldo.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(UserUtil.getInstance(this).getSaldo())).replace(',','.')));

        if(Integer.parseInt(UserUtil.getInstance(this).getSaldo()) > tagihan.getJumlahTagihan()){
            saldoCukup = true;
        }

        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(saldoCukup){
                    paymentTagihan();
                }else{
                    alert();
                }
            }
        });
    }

    private void alert(){
        new AlertDialog.Builder(this)
                .setTitle("Saldo Tidak Cukup")
                .setMessage("Silahkan isi ulang saldo")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()){
            case android.R.id.home :
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ProgressDialog mProgressBar;
    private void paymentTagihan(){
        mProgressBar = new ProgressDialog(this);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                        finish();
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        getSaldo();
                        tagihanTable.insert(tagihan);
                        Toast.makeText(PaymentActivity.this, R.string.payment_success, Toast.LENGTH_LONG).show();
                    }else{
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                mProgressBar.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                mProgressBar.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                mProgressBar.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                mProgressBar.show();
            }
        };

        RestClient.getInstance(this, tagihanResponseHandler).postTagihan(UserUtil.getInstance(this).getToken(), Integer.parseInt(tagihan.getIdTag()), true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setSaldo(String saldo){
        UserUtil.getInstance(this).setSaldo(saldo);
    }

    private void getSaldo(){
        JsonHttpResponseHandler saldoResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        setSaldo(response.getString("saldo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(this, saldoResponseHandler).getSaldo(UserUtil.getInstance(this).getToken(), true);
    }
}
