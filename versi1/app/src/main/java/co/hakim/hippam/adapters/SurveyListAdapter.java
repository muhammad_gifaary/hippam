/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class SurveyListAdapter extends RecyclerView.Adapter<SurveyListAdapter.SurveyListViewHolder>{

    private static final String TAG = SurveyListAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Survey> surveyList;

    private Context context;

    public SurveyListAdapter(List<Survey> surveyList, Context context) {
        this.surveyList = surveyList;
        this.context = context;
    }

    @Override
    public SurveyListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SurveyListViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_survey_new, parent, false));
    }

    @Override
    public void onBindViewHolder(SurveyListViewHolder holder, int position) {
        Survey survey = surveyList.get(position);
        holder.year.setText(DateTimeUtil.format(survey.getInputDate(), Constants.DATE_YEAR));
        holder.month.setText(DateTimeUtil.format(survey.getInputDate(), Constants.DATE_MONTH));
        holder.volume.setText(Html.fromHtml( survey.getInputMeter() + " M<sup><small>3<small></sup>"));

        int color;
        String status;
        if(survey.getSyncStatus().equals(Constants.SYNC_STATUS_PENDING)){
            status = "Pending";
            color = ContextCompat.getColor(context, R.color.red);
        }else{
            status = "Uploaded";
            color = ContextCompat.getColor(context, R.color.limegreen);
        }

        holder.status.setText(status);
        holder.status.setTextColor(color);
        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(SurveyListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(surveyList != null){
            return surveyList.size();
        }

        return 0;
    }

    public void swapData(List<Survey> listSurvey){
        if(surveyList != null){
            surveyList = listSurvey;
        }

        notifyDataSetChanged();
    }

    public class SurveyListViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView month, volume, year, status;
        public SurveyListViewHolder(View itemView) {
            super(itemView);
            container = (View) itemView.findViewById(R.id.container);
            month = (TextView) itemView.findViewById(R.id.month);
            year = (TextView) itemView.findViewById(R.id.year);
            volume = (TextView) itemView.findViewById(R.id.volume);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
