/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.second_version;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;
import co.hakim.hippam.fragments.InformationFragment;
import co.hakim.hippam.fragments.PengaduanFragment;
import co.hakim.hippam.fragments.TagihanUserFragment;
import co.hakim.hippam.utils.UserUtil;

public class UserActivity extends AppCompatActivity {

    private NavigationPagerAdapter navigationPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        navigationPagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(navigationPagerAdapter);

        TextView enumName = (TextView) findViewById(R.id.enum_name);
        TextView pam = (TextView) findViewById(R.id.group);
        TextView address = (TextView) findViewById(R.id.address);

        enumName.setText(UserUtil.getInstance(this).getFullName());
        address.setText(UserUtil.getInstance(this).getAddress());

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        TextView logout = (TextView) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout(){
        UserUtil.getInstance(this).signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class NavigationPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;
        private String[] titles;

        public NavigationPagerAdapter(FragmentManager fm, Context context) {
            super(fm);

            fragments = new ArrayList<>();
            fragments.add(new TagihanUserFragment());
            fragments.add(new InformationFragment());
            fragments.add(new PengaduanFragment());

            titles = context.getResources().getStringArray(R.array.label_user);
        }

        @Override
        public Fragment getItem(int position) {
            if (position < 0 || position >= fragments.size()) {
                Toast.makeText(getBaseContext(), "Unknown menu", Toast.LENGTH_SHORT).show();
                return null;
            }

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position < titles.length) {
                return titles[position];
            }

            return null;
        }
    }
}
