/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.db.tables;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.domains.providers.HippamContentProviders;
import co.hakim.hippam.utils.DateTimeUtil;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class SurveyTable {
    private static final String TAG = SurveyTable.class.getSimpleName();

    public static final String TABLE_NAME = "_survey";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CUSTOMER_ID = "_customer_id";
    public static final String COLUMN_DATE = "_date";
    public static final String COLUMN_METER = "_input_meter";
    public static final String COLUMN_SYNC_DATE = "_sync_date";
    public static final String COLUMN_SYNC_STATUS = "_sync_status";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, COLUMN_CUSTOMER_ID, COLUMN_METER, COLUMN_DATE, COLUMN_SYNC_DATE, COLUMN_SYNC_STATUS
    };

    private static final Uri URI = HippamContentProviders.URI_SURVEYS;
    private static final String CREATE_TABLE;
    private ContentResolver contentResolver;

    static {
        CREATE_TABLE = new StringBuilder().append("create table ")
                .append(TABLE_NAME)
                .append(" (")
                .append(COLUMN_ID)
                .append(" integer primary key autoincrement, ")
                .append(COLUMN_CUSTOMER_ID)
                .append(" text not null, ")
                .append(COLUMN_METER)
                .append(" integer not null, ")
                .append(COLUMN_DATE)
                .append(" text not null, ")
                .append(COLUMN_SYNC_STATUS)
                .append(" text not null, ")
                .append(COLUMN_SYNC_DATE)
                .append(" text not null)")
                .toString();
    }

    private Context context;

    public SurveyTable(Context context) {
        this.context = context;
    }

    public SurveyTable(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public static void setValues(Cursor cursor, Survey survey) {
        survey.setCustomerId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CUSTOMER_ID)));
        survey.setInputMeter(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_METER)));
        survey.setInputDate(DateTimeUtil.parse(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE))));
        survey.setSyncDate(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SYNC_DATE)));
        survey.setSyncStatus(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SYNC_STATUS)));
    }

    public static void setValues(Survey survey, ContentValues values) {
        values.put(COLUMN_CUSTOMER_ID, survey.getCustomerId());
        values.put(COLUMN_METER, survey.getInputMeter());
        values.put(COLUMN_DATE, DateTimeUtil.formatDBDateOnly(survey.getInputDate()));
        values.put(COLUMN_SYNC_DATE, survey.getSyncDate());
        values.put(COLUMN_SYNC_STATUS, survey.getSyncStatus());
    }

    public void updateSurveyStatus(String id, String status) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SYNC_STATUS, status);
        updateSurvey(id, values);
    }

    public void updateSurvey(String id, ContentValues values) {
        if (values != null) {
            String selection = "strftime('%m'," + COLUMN_DATE + ") = '" + DateTimeUtil.format(DateTimeUtil.now(), "MM") + "'";

            Uri uri = Uri.parse(URI + "/" + id);
            contentResolver.update(uri, values, selection, null);
        }
    }

    /*
    * delete survey
    */
    public void delete(int surveyId){
        if (Constants.DEBUG) {
            Log.d(TAG, "updateById, id = " + surveyId);
        }

        String selection = "strftime('%m'," + COLUMN_DATE + ") = '" + DateTimeUtil.format(DateTimeUtil.now(), "MM") + "'";

        Uri uri = Uri.parse(URI + "/" + surveyId);
        contentResolver.delete(uri, selection, null);
    }

    public void insert(Survey survey) {
        if (Constants.DEBUG) {
            Log.d(TAG, "insert customer");
            Log.d(TAG, "data = " + survey.getCustomerId() + " " + survey.getInputMeter());
        }

        ContentValues values = new ContentValues();
        setValues(survey, values);
        context.getContentResolver().insert(URI, values);
    }

    public Survey getById(String surveyId) {

        if (Constants.DEBUG) {
            Log.d(TAG, "getByNumberId, id = " + surveyId);
        }

        Uri uri = Uri.parse(URI + "/" + surveyId);
        Cursor c = context.getContentResolver().query(uri, ALL_COLUMNS, "strftime('%m'," + COLUMN_DATE + ") = ?",
                new String[]{DateTimeUtil.format(DateTimeUtil.now(), "MM")}, null);

        if (c != null && c.moveToFirst()) {
            Survey s = new Survey();
            setValues(c, s);
            c.close();
            return s;
        }

        return null;
    }

    public Survey getByIdAndDate(String surveyId, Date date) {

        if (Constants.DEBUG) {
            Log.d(TAG, "getByNumberId, id = " + surveyId);
        }

        Uri uri = Uri.parse(URI + "/" + surveyId);
        Cursor c = context.getContentResolver().query(uri, ALL_COLUMNS, "strftime('%m'," + COLUMN_DATE + ") = ?",
                new String[]{DateTimeUtil.format(date, "MM")}, null);

        if (c != null && c.moveToFirst()) {
            Survey s = new Survey();
            setValues(c, s);
            c.close();
            return s;
        }

        return null;
    }

    public List<Survey> getByMonth(String month) {

        List<Survey> surveyList = new ArrayList<>();

        Cursor c = context.getContentResolver().query(URI, ALL_COLUMNS, "strftime('%m'," + COLUMN_DATE + ") = ?",
                new String[]{month}, null);

        if (c == null)
            return surveyList;

        if (c.moveToFirst()) {
            do {
                Survey s = new Survey();
                setValues(c, s);
                surveyList.add(s);
            } while (c.moveToNext());
        }
        c.close();

        return surveyList;
    }

    public List<Survey> getByCustomerId(String surveyid) {

        List<Survey> surveyList = new ArrayList<>();
        if (Constants.DEBUG) {
            Log.d(TAG, "getByNumberId, id = " + surveyid);
        }

        Uri uri = Uri.parse(URI + "/" + surveyid);
        Cursor c = context.getContentResolver().query(uri, ALL_COLUMNS, null, null, null);

        if(c == null)
            return surveyList;

        if (c.moveToFirst()) {
            do {
                Survey s = new Survey();
                setValues(c, s);
                surveyList.add(s);
            } while (c.moveToNext());
        }
        c.close();

        return surveyList;
    }

    public boolean isEmpty() {

        Cursor c = context.getContentResolver().query(URI, new String[]{SurveyTable.COLUMN_CUSTOMER_ID}, null, null, null);
        if (c == null)
            return true;

        if (c.moveToFirst()) {
            c.close();
            return false;
        }

        return true;
    }

    public List<Survey> getPendingData() {

        List<Survey> surveyList = new ArrayList<>();

        Cursor c = contentResolver.query(
                URI,
                ALL_COLUMNS,
                COLUMN_SYNC_STATUS + " = ?",
                new String[]{Constants.SYNC_STATUS_PENDING},
                COLUMN_SYNC_DATE);

        if (c == null)
            return surveyList;

        if (c.moveToFirst()) {
            do {
                Survey s = new Survey();
                setValues(c, s);
                surveyList.add(s);
            } while (c.moveToNext());
        }
        c.close();

        return surveyList;
    }

    public List<Survey> getAll() {

        List<Survey> surveyList = new ArrayList<>();

        Cursor c = context.getContentResolver().query(URI, ALL_COLUMNS, null, null, COLUMN_CUSTOMER_ID);
        if (c == null)
            return surveyList;

        if (c.moveToFirst()) {
            do {
                Survey s = new Survey();
                setValues(c, s);
                surveyList.add(s);
            } while (c.moveToNext());
        }
        c.close();

        return surveyList;
    }

    public List<Survey> getGroupSurvey() {

        List<Survey> surveyList = new ArrayList<>();

        Cursor c = context.getContentResolver().query(HippamContentProviders.URI_SURVEYS_GROUP, ALL_COLUMNS, null, null, COLUMN_CUSTOMER_ID);
        if (c == null)
            return surveyList;

        if (c.moveToFirst()) {
            do {
                Survey s = new Survey();
                setValues(c, s);
                surveyList.add(s);
            } while (c.moveToNext());
        }
        c.close();

        return surveyList;
    }
}
