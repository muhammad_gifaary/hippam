/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.TagihanListAdapter;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class DetailTagihanActivity extends AppCompatActivity implements ItemClickListener {

    private static final String TAG = DetailTagihanActivity.class.getSimpleName();

    private RecyclerView listTagihan;
    private TagihanListAdapter tagihanListAdapter;
    private DotProgressBar progressBar;
    private ImageView notFound;
    private List<Tagihan> tagihanList;
    private String idPel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tagihan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.detail_tagihan);

        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tagihanList = new ArrayList<>();

        idPel = getIntent().getStringExtra(Constants.EXTRA_NOPEL);
        listTagihan = (RecyclerView) findViewById(R.id.list_tagihan);
        progressBar = (DotProgressBar) findViewById(R.id.progress_bar);
        notFound = (ImageView) findViewById(R.id.not_found);
        tagihanListAdapter = new TagihanListAdapter(tagihanList, this, this);
        listTagihan.setLayoutManager(new LinearLayoutManager(this));
        listTagihan.setItemAnimator(new DefaultItemAnimator());
        listTagihan.setAdapter(tagihanListAdapter);

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()){
            case android.R.id.home :
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loadData(){
        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(tagihanList.size() > 0)
                        tagihanList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");
                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Tagihan tagihan = new Tagihan();
                            tagihan.setIdTag(data.getString("id_tag"));
                            tagihan.setNoPel(data.getString("nopel"));
                            tagihan.setNamaPel(data.getString("nama"));
                            tagihan.setAlamatPel(data.getString("alamat"));
                            tagihan.setTagihanBulan(data.getString("tagihan"));
                            tagihan.setJumlahTagihan(data.getInt("jumlah"));

                            tagihanList.add(tagihan);
                        }

                        tagihanListAdapter.swapData(tagihanList);
                        listTagihan.setVisibility(View.VISIBLE);
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressBar.setVisibility(View.GONE);
            }
        };

        RestClient.getInstance(this, tagihanResponseHandler).getTagihan(idPel, UserUtil.getInstance(this).getToken(), true);
    }

    @Override
    public void onItemClicked(int position, View view) {
        Tagihan tagihan = tagihanListAdapter.getTagihan(position);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(Constants.EXTRA_TAGIHAN, tagihan);
        startActivity(intent);
    }
}
