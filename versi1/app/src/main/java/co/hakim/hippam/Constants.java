package co.hakim.hippam;

/**
 * Created by Hakim on 27-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class Constants {
    public static final boolean DEVELOPMENT = true;
    public static final boolean DEBUG = true;
    public static final String APP_ROOT = "co.hakim.hippam";

    public static final String USER_EMAIL = "email";
    public static final String USER_FULL_NAME = "nama_user";
    public static final String USER_TOKEN = "token_key";
    public static final String USER_STATUS = "status";
    public static final String USER_SALDO = "saldo";
    public static final String USER_INST = "instansi";
    public static final String USER_ADDRESS = "alamat";
    public static final String JSON_USER_TOKEN = "token_key";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_USER_FULL_NAME = "nama_user";
    public static final String JSON_USER_STATUS = "status";
    public static final String JSON_USER_ADDRESS = "alamat";

    public static final int USER_TYPE_ENUMERATOR_A = 1;
    public static final int USER_TYPE_ENUMERATOR_B = 3;
    public static final int USER_TYPE_LOKET = 2;
    public static final int USER_TYPE_CUSTOMER = 4;

    public static final String EXTRA_LOGIN_TYPE = APP_ROOT + ".LoginType";
    public static final String EXTRA_CUSTOMER = APP_ROOT + ".Customer";
    public static final String EXTRA_INPUT_DONE = APP_ROOT + ".InputDone";
    public static final String EXTRA_NOPEL = APP_ROOT + ".noPelanggan";
    public static final String EXTRA_TAGIHAN = APP_ROOT + ".Tagihan";
    public static final String EXTRA_REKENING = APP_ROOT + ".Rekening";

    public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_MID_MONTH_FORMAT = "dd MMM yy";
    public static final String DATE_DATEPICKER_FORMAT = "dd MMM yyyy";
    public static final String DATE_MONTH_FORMAT = "dd MMMM yyyy";
    public static final String DATE_DAY_NAME_FORMAT = "EEEE, dd MMMM yyyy";
    public static final String DATE_TIME_DAY_NAME_FORMAT = "EEEE, dd MMMM yyyy HH:mm:ss";
    public static final String DATE_COMMENT_FORMAT = "dd/MM/yyyy HH:mm";
    public static final String DATE_CHART_FORMAT = "MMMM dd, yyyy";
    public static final String DATE_MID_MONTH = "dd MMM";
    public static final String DATE_MONTH_YEAR = "MMMM yyyy";
    public static final String DATE_MONTH = "MMMM";
    public static final String DATE_YEAR = "yyyy";
    public static final int DATA_FETCH_LENGTH = 10;

    public static final String SYNC_STATUS_PENDING = "pending";
    public static final String SYNC_STATUS_SENDING = "sending";
    public static final String SYNC_STATUS_SENT = "sent";
    public static final String SYNC_STATUS_FAILED = "failed";

    public static final String INTENT_ACTION_SYNC = "co.hakim.hippam.SYNC";
    public static final String EXTRA_SYNC_PROGRESS = "sync_progress";
    public static final int SYNC_PROGRESS_FINISHED = 0;
    public static final int SYNC_PROGRESS_STARTED = 1;

}
