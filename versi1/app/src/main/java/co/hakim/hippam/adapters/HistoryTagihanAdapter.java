/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.DateTimeUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by Hakim on 30-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class HistoryTagihanAdapter extends RecyclerView.Adapter<HistoryTagihanAdapter.HistoryTagihanViewHolder> {

    private static final String TAG = HistoryTagihanAdapter.class.getSimpleName();

    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private Context context;

    public HistoryTagihanAdapter(Context context, List<Tagihan> tagihanList) {
        this.context = context;
        this.tagihanList = tagihanList;
    }

    @Override
    public HistoryTagihanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryTagihanViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_customer, parent, false));
    }

@Override
    public void onBindViewHolder(HistoryTagihanViewHolder holder, int position) {
        Tagihan tagihan = tagihanList.get(position);
        holder.namaPel.setText(tagihan.getNamaPel());
        holder.tanggalTransc.setText(DateTimeUtil.format(DateTimeUtil.parse(tagihan.getTanggalBayar()), Constants.DATE_MONTH_FORMAT));
        holder.jumlahTranc.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));
        holder.statusTransc.setText(tagihan.getTagihanBulan());

        setAnimation(holder.container, position);
    }


    public void onViewDetachedFromWindow(HistoryTagihanViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(tagihanList != null){
            return tagihanList.size();
        }

        return 0;
    }

    public class HistoryTagihanViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView namaPel, tanggalTransc, jumlahTranc, statusTransc;
        public HistoryTagihanViewHolder(View itemView) {
            super(itemView);
            container = (View) itemView.findViewById(R.id.container);
            namaPel = (TextView) itemView.findViewById(R.id.customer_name);
            tanggalTransc = (TextView) itemView.findViewById(R.id.transaction_date);
            jumlahTranc = (TextView) itemView.findViewById(R.id.jumlah_trans);
            statusTransc = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
