package co.hakim.hippam.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.hakim.hippam.Constants;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class DateTimeUtil {
    public static Date parse(String dateStr, String format) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            Log.e("DateUtil.parse", e.getMessage(), e);
        }
        return null;
    }

    public static Date parse(String dateStr, boolean dateOnly) {
        if (dateOnly) {
            return parse(dateStr, Constants.DATE_DEFAULT_FORMAT);
        } else {
            return parse(dateStr, Constants.DATE_TIME_DEFAULT_FORMAT);
        }
    }

    public static Date parse(String dateStr) {
        return parse(dateStr, true);
    }

    /**
     * @param date   Date to format.
     * @param format Format string.
     * @return Formatted date.
     */
    public static String format(Date date, String format) {
        return format(date, format, Locale.getDefault());
    }

    public static String format(Date date, String format, Locale locale) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, locale);
            return dateFormat.format(date);
        } catch (Exception e) {
            Log.e("DateUtil.parse", e.getMessage(), e);
        }

        return null;
    }

    public static String formatDBDateOnly(Date date) {
        return format(date, Constants.DATE_DEFAULT_FORMAT);
    }

    public static String formatDBDateTime(Date date) {
        return format(date, Constants.DATE_TIME_DEFAULT_FORMAT);
    }

    public static String formatDateMidMonth(Date date) {
        return format(date, Constants.DATE_MID_MONTH_FORMAT);
    }

    public static Date now() {
        return Calendar.getInstance().getTime();
    }

    public static Date daysAgo(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1 * day);
        return cal.getTime();

    }

    public static Date monthAgo(int month){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1 * month);
        return cal.getTime();
    }
}
