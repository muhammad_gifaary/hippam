/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.activities.second_version.NewFeedbackActivity;
import co.hakim.hippam.adapters.ReportListAdapter;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

public class FeedbackListActivity extends AppCompatActivity {

    private static final String TAG = FeedbackListActivity.class.getSimpleName();

    private List<Report> reportList;
    private DotProgressBar progressBar;
    private ImageView notFound;
    private RecyclerView listReportRecyclerView;
    private ReportListAdapter reportListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.list_report);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reportList = new ArrayList<>();
        progressBar = (DotProgressBar) findViewById(R.id.progress_bar);
        notFound = (ImageView) findViewById(R.id.not_found);

        listReportRecyclerView = (RecyclerView) findViewById(R.id.feedback_list);
        reportListAdapter = new ReportListAdapter(reportList, this);
        listReportRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        listReportRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listReportRecyclerView.setAdapter(reportListAdapter);

        loadData();
    }

    private void loadData(){
            JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(reportList.size() > 0)
                        reportList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");

                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Report report = new Report();
                            report.setId(data.getInt("id"));
                            report.setProgress(data.getString("progres"));
                            report.setReport(data.getString("uraian_aduan"));

                            reportList.add(report);
                        }

                        reportListAdapter.swapData(reportList);
                        listReportRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressBar.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStart() {
                super.onStart();
                progressBar.setVisibility(View.VISIBLE);
            }

                @Override
            public void onFinish() {
                super.onFinish();
                progressBar.setVisibility(View.GONE);
            }
        };

        RestClient.getInstance(this, tagihanResponseHandler).getListReport(UserUtil.getInstance(this).getToken(), true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.tambah:
                Intent intent = new Intent(this, NewFeedbackActivity.class);
                startActivity(intent);
                break;
            case android.R.id.home :
                onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
