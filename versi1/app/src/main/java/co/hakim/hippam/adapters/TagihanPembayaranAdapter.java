/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.DataLoadListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by bigio on 11/6/16.
 */

public class TagihanPembayaranAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = TagihanPembayaranAdapter.class.getSimpleName();

    public static final int VIEW_PROGRESS = 0;
    public static final int VIEW_ITEM = 1;
    private DataLoadListener loadListener;
    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private Context context;

    public TagihanPembayaranAdapter(List<Tagihan> tagihanList, Context context, DataLoadListener dataLoadListener) {
        this.tagihanList = tagihanList;
        this.context = context;
        this.loadListener = dataLoadListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        if (viewType == VIEW_ITEM) {
            if (Constants.DEBUG) Log.d(TAG, "item row");
            viewHolder = new PembayaranTagihanViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pembayaran, parent, false));
        } else {
            if (Constants.DEBUG) Log.d(TAG, "progress row");
            View rootView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_progress, parent, false);
            viewHolder = new ProgressBarViewHolder(rootView);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vHolder, int position) {
        View container;
        if(vHolder instanceof PembayaranTagihanViewHolder){
            Tagihan tagihan = tagihanList.get(position);
            PembayaranTagihanViewHolder viewHolder = (PembayaranTagihanViewHolder) vHolder;
            viewHolder.monthYear.setText(tagihan.getTagihanBulan());
            viewHolder.payment.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));
            viewHolder.status.setText(tagihan.getStatusTagihan());

            if (position == getItemCount() - 2 && loadListener != null) {
                loadListener.onLastData();
            }

            container = viewHolder.container;
        }else{
            ProgressBarViewHolder viewHolder = (ProgressBarViewHolder) vHolder;
            container = viewHolder.getContainer();
        }

        setAnimation(container, position);
    }

    @Override
    public int getItemViewType(int position) {
        return tagihanList.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(tagihanList == null){
            return 0;
        }

        return tagihanList.size();
    }

    public void remove(int position) {
        if (tagihanList != null) {
            tagihanList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Tagihan getTagihan(int position){
        return tagihanList.get(position);
    }

    public void swapData(List<Tagihan> tagihanList){
        if(tagihanList != null){
            this.tagihanList = tagihanList;
        }

        notifyDataSetChanged();
    }

    public void add(Tagihan tagihan){
        if(tagihanList == null){
            tagihanList = new ArrayList<>();
        }

        tagihanList.add(tagihan);
        notifyItemInserted(tagihanList.size() - 1);
    }

    public void clearItems() {
        if (Constants.DEBUG) Log.d(TAG, "clearing items");
        if (tagihanList != null) {
            tagihanList.clear();
            notifyDataSetChanged();
        }
    }

    public class PembayaranTagihanViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView monthYear, payment, status;
        public PembayaranTagihanViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            monthYear = (TextView) itemView.findViewById(R.id.month_year);
            payment = (TextView) itemView.findViewById(R.id.tagihan);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
