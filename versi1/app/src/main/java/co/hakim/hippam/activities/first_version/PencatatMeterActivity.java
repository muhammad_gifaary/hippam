/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.activities.first_version;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import co.hakim.hippam.R;
import co.hakim.hippam.activities.LoginActivity;
import co.hakim.hippam.fragments.PencatatMeterFragment;
import co.hakim.hippam.utils.SyncUtil;
import co.hakim.hippam.utils.UserUtil;

public class PencatatMeterActivity extends AppCompatActivity{

    private static final String TAG = PencatatMeterActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencatat_meter);

        TextView enumName = (TextView) findViewById(R.id.enum_name);
        enumName.setText(UserUtil.getInstance(this).getFullName());

        TextView logout = (TextView) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });


        getSupportFragmentManager().beginTransaction().add(R.id.pencatat_meter_fragment, new
                PencatatMeterFragment()).commit();

        SyncUtil.createSyncAccount(this);
    }

    private void logout(){
        UserUtil.getInstance(this).signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
