/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.domains.models.Survey;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Hakim on 29-Oct-16.
 *
 * @author by Hakim
 *         <p/>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = SyncAdapter.class.getSimpleName();
    private Context mContext;
    private ContentResolver mContentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
        mContentResolver = context.getContentResolver();
    }

    public SyncAdapter(Context context, boolean autoInitialize,
                       boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account,
                              Bundle extras,
                              String authority,
                              ContentProviderClient provider,
                              SyncResult syncResult) {
        if (Constants.DEBUG) Log.i(TAG, "sync performed");

        Intent intent = new Intent(Constants.INTENT_ACTION_SYNC);
        intent.putExtra(Constants.EXTRA_SYNC_PROGRESS, Constants.SYNC_PROGRESS_STARTED);
        mContext.sendBroadcast(intent);
            pushSurvey();
        intent.putExtra(Constants.EXTRA_SYNC_PROGRESS, Constants.SYNC_PROGRESS_FINISHED);
        mContext.sendBroadcast(intent);

        if (Constants.DEBUG) Log.i(TAG, "sync finished");
    }

    private void pushSurvey(){
        uploadInputMeter(getPendingSurvey());
    }

    private List<Survey> getPendingSurvey(){
        if (Constants.DEBUG) Log.d(TAG, "looking for pending survey");
        SurveyTable surveyTable = new SurveyTable(mContentResolver);
        return surveyTable.getPendingData();
    }

    private void uploadInputMeter(List<Survey> surveys){
        final SurveyTable surveyTable = new SurveyTable(mContentResolver);

        for (final Survey survey : surveys){

            JsonHttpResponseHandler surveyUploadHandler = new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (Constants.DEBUG) {
                            Log.d(TAG, "Survey response = " + response.toString(4));
                        }

                        if (RestResponseHandler.isSuccess(response)) {
                            surveyTable.updateSurveyStatus(survey.getCustomerId(), Constants.SYNC_STATUS_SENT);
                        } else {
                            if(response.getString("msg").equals("Data input sudah dilakukan")){
                                surveyTable.updateSurveyStatus(survey.getCustomerId(), Constants.SYNC_STATUS_SENT);
                            }
                            throw new Exception("Upload Survey Failed");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "distributor response = " + response.toString());
                        Log.e(TAG, e.getMessage(), e);
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    if (errorResponse != null) {
                        Log.e(TAG, errorResponse.toString());
                    }

                    Log.e(TAG, throwable.getMessage(), throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);

                    if (!TextUtils.isEmpty(responseString)) {
                        Log.e(TAG, responseString);
                    }

                    Log.e(TAG, throwable.getMessage(), throwable);
                }
            };


            RestClient.getInstance(mContext, surveyUploadHandler).postSurvey(UserUtil.getInstance(mContext).getToken(), survey.getCustomerId(), survey.getInputMeter(), false);
        }
    }
}
