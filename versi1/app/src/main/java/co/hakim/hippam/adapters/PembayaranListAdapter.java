/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.adapters;

import android.content.Context;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.utils.AnimationUtil;
import co.hakim.hippam.utils.DateTimeUtil;
import co.hakim.hippam.utils.Formatter;

/**
 * Created by bigio on 11/5/16.
 */

public class PembayaranListAdapter extends RecyclerView.Adapter<PembayaranListAdapter.PembayaranViewHolder>{

    private int lastPosition = -1;
    private List<Tagihan> tagihanList;
    private Context context;
    private ItemClickListener itemClickListener;

    public PembayaranListAdapter(List<Tagihan> tagihanList, Context context, ItemClickListener itemClickListener) {
        this.tagihanList = tagihanList;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public PembayaranViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return  new PembayaranViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.row_pembayaran, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(PembayaranViewHolder holder, int position) {
        Tagihan tagihan = tagihanList.get(position);

        holder.monthYear.setText(tagihan.getTagihanBulan());
        holder.payment.setText(String.format("Rp %s", Formatter.doubleValue(Double.parseDouble(String.valueOf(tagihan.getJumlahTagihan()))).replace(',','.')));


        setAnimation(holder.container, position);
    }

    public void onViewDetachedFromWindow(PembayaranViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.container.clearAnimation();
    }

    private void setAnimation(View view, int position) {
        // Only animate newly visible view, previously visible ones not included
        if (position > lastPosition) {
            AnimationUtil.fadeIn(context, view);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if(tagihanList.size() > 0){
            return tagihanList.size();
        }

        return 0;
    }

    public Tagihan getTagihan(int position){
        return tagihanList.get(position);
    }

    public void swapData(List<Tagihan> tagihanList){
        if(tagihanList != null){
            this.tagihanList = tagihanList;
        }

        notifyDataSetChanged();
    }

    public class PembayaranViewHolder extends RecyclerView.ViewHolder{

        private View container;
        private TextView monthYear, payment;
        private LinearLayout pay;
        public PembayaranViewHolder(final View itemView, final ItemClickListener itemClickListener) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            monthYear = (TextView) itemView.findViewById(R.id.month_year);
            payment = (TextView) itemView.findViewById(R.id.tagihan);
            pay = (LinearLayout) itemView.findViewById(R.id.bayar);
            pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        itemClickListener.onItemClicked(getAdapterPosition(), itemView);
                    }
                }
            });
        }
    }
}
