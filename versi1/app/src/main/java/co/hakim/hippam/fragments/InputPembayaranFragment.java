/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.ItemClickListener;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.PembayaranListAdapter;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.db.tables.TagihanTable;
import co.hakim.hippam.domains.models.Customer;
import co.hakim.hippam.domains.models.Tagihan;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputPembayaranFragment extends Fragment implements ItemClickListener{

    private static final String TAG = InputPembayaranFragment.class.getSimpleName();

    private TagihanTable tagihanTable;
    private TextInputLayout inputCustomerIdField;
    private EditText inputCustId;
    private LinearLayout containerInputNomor, containerDetailPelanggan, backInput;
    private TextView name, address, id, group;
    private PembayaranListAdapter pembayaranListAdapter;
    private List<Tagihan> tagihanList;
    private RecyclerView recyclerView;
    private ImageView notFound;
    private Customer customer;

    private Context context;

    public InputPembayaranFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_input_pembayaran, container, false);

        tagihanTable = new TagihanTable(context);
        tagihanList = new ArrayList<>();
        customer = new Customer();

        containerInputNomor = (LinearLayout) view.findViewById(R.id.container_input_nomor);
        containerDetailPelanggan = (LinearLayout) view.findViewById(R.id.detail_pelanggan);
        name = (TextView) view.findViewById(R.id.customer_name);
        address = (TextView) view.findViewById(R.id.customer_address);
        id = (TextView) view.findViewById(R.id.customer_id);
        group = (TextView) view.findViewById(R.id.customer_group);

        inputCustomerIdField = (TextInputLayout) view.findViewById(R.id.inputmeter_field);
        inputCustId = (EditText) view.findViewById(R.id.input_meter);

        notFound = (ImageView) view.findViewById(R.id.not_found);

        pembayaranListAdapter = new PembayaranListAdapter(tagihanList, context, this);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_pembayaran);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pembayaranListAdapter);

        backInput = (LinearLayout) view.findViewById(R.id.back_input);
        backInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                containerInputNomor.setVisibility(View.VISIBLE);
                containerDetailPelanggan.setVisibility(View.GONE);
            }
        });

        Button checkTag = (Button) view.findViewById(R.id.check_tagihan);
        checkTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(inputCustId.getText().toString())){
                    if (!inputCustomerIdField.isErrorEnabled()) inputCustomerIdField.setErrorEnabled(true);
                    inputCustomerIdField.setError(getString(R.string.error_field_required));
                    inputCustId.requestFocus();
                }else {
                    detailPelanggan(inputCustId.getText().toString());
                }
            }
        });

        return view;
    }

    private void setData(Customer customer){
        name.setText(customer.getCustomerName());
        address.setText(customer.getCustomerAddress());
        id.setText(customer.getCustomerId());
        group.setText(customer.getCustomerGroup());

        loadData(customer.getCustomerId());
    }

    private void transition(){
        if(customer != null){
            setData(customer);

            containerInputNomor.setVisibility(View.GONE);
            InputMethodManager im = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            containerDetailPelanggan.setVisibility(View.VISIBLE);
        }else{
            if (!inputCustomerIdField.isErrorEnabled()) inputCustomerIdField.setErrorEnabled(true);
            inputCustomerIdField.setError(getString(R.string.user_not_found));
            inputCustId.requestFocus();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        containerInputNomor.setVisibility(View.VISIBLE);
        containerDetailPelanggan.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(int position, View view) {
        Tagihan tagihan = pembayaranListAdapter.getTagihan(position);
        paymentTagihan(tagihan);
//        if(Integer.parseInt(UserUtil.getInstance(context).getSaldo()) > tagihan.getJumlahTagihan()){
//            paymentTagihan(tagihan);
//        }else{
//            alert();
//        }
    }

    private void alert(){
        new AlertDialog.Builder(context)
                .setTitle("Saldo Tidak Cukup")
                .setMessage("Silahkan isi ulang saldo")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void loadData(String idPel){
        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(tagihanList.size() > 0)
                        tagihanList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");
                        for (int i=0; i<datum.length(); i++){
                            JSONObject data = datum.getJSONObject(i);
                            Tagihan tagihan = new Tagihan();
                            tagihan.setIdTag(data.getString("id_tag"));
                            tagihan.setNoPel(data.getString("nopel"));
                            tagihan.setNamaPel(data.getString("nama"));
                            tagihan.setAlamatPel(data.getString("alamat"));
                            tagihan.setTagihanBulan(data.getString("tagihan"));
                            tagihan.setJumlahTagihan(data.getInt("jumlah"));

                            tagihanList.add(tagihan);
                        }

                        pembayaranListAdapter.swapData(tagihanList);
                        recyclerView.setVisibility(View.VISIBLE);
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, tagihanResponseHandler).getTagihan(idPel, UserUtil.getInstance(context).getToken(), true);
    }

    private void detailPelanggan(String idPel){
        JsonHttpResponseHandler detailResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");
                        CustomerTable.setValues(datum.getJSONObject(0), customer);
                        transition();
                    }else{
                        if (!inputCustomerIdField.isErrorEnabled()) inputCustomerIdField.setErrorEnabled(true);
                        inputCustomerIdField.setError(getString(R.string.user_not_found));
                        inputCustId.requestFocus();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, detailResponseHandler).getDetailPelanggan(UserUtil.getInstance(context).getToken(),idPel, true);
    }

    private ProgressDialog mProgressBar;
    private void paymentTagihan(final Tagihan tagihan){
        mProgressBar = new ProgressDialog(context);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        JsonHttpResponseHandler tagihanResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(RestResponseHandler.isSuccess(response)){
                        getSaldo();
                        tagihan.setSisaSaldo(Integer.parseInt(UserUtil.getInstance(context).getSaldo()));
                        tagihanTable.insert(tagihan);
                        containerInputNomor.setVisibility(View.VISIBLE);
                        containerDetailPelanggan.setVisibility(View.GONE);
                    }else{
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                mProgressBar.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                mProgressBar.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                mProgressBar.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                mProgressBar.show();
            }
        };

        RestClient.getInstance(context, tagihanResponseHandler).postTagihan(UserUtil.getInstance(context).getToken(), Integer.parseInt(tagihan.getIdTag()), true);
    }

    private void setSaldo(String saldo){
        UserUtil.getInstance(context).setSaldo(saldo);
    }

    private void getSaldo(){
        JsonHttpResponseHandler saldoResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if(RestResponseHandler.isSuccess(response)){
                        setSaldo(response.getString("saldo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, saldoResponseHandler).getSaldo(UserUtil.getInstance(context).getToken(), true);
    }
}
