/*
 * Copyright (c) 2016.  Hakim Marsudi - hakimmarsudi.co
 */

package co.hakim.hippam.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.hakim.hippam.Constants;
import co.hakim.hippam.R;
import co.hakim.hippam.adapters.InformationAdapter;
import co.hakim.hippam.adapters.ReportListAdapter;
import co.hakim.hippam.domains.models.Report;
import co.hakim.hippam.net.RestClient;
import co.hakim.hippam.net.RestResponseHandler;
import co.hakim.hippam.utils.UserUtil;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationFragment extends Fragment {

    private static final String TAG = InformationAdapter.class.getSimpleName();

    private ImageView notFound;
    private RecyclerView informationRecyclerView;
    private InformationAdapter informationAdapter;
    private Context context;
    private List<String> stringList;

    public InformationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        stringList = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_information, container, false);
        notFound = (ImageView) view.findViewById(R.id.not_found);
        informationRecyclerView = (RecyclerView) view.findViewById(R.id.list_information);
        informationAdapter = new InformationAdapter(stringList, context);
        informationRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        informationRecyclerView.setItemAnimator(new DefaultItemAnimator());
        informationRecyclerView.setAdapter(informationAdapter);

        loadData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void loadData(){
        JsonHttpResponseHandler infResponseHandler = new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (Constants.DEBUG) {
                        Log.d(TAG, "response = " + response.toString(4));
                    }

                    if(stringList.size() > 0)
                        stringList.clear();

                    if(RestResponseHandler.isSuccess(response)){
                        JSONArray datum = response.getJSONArray("data");

                        for (int i=0; i<datum.length(); i++){

                        }

                        informationAdapter.swapData(stringList);
                        informationRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        notFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                notFound.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                notFound.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        RestClient.getInstance(context, infResponseHandler).getListReport(UserUtil.getInstance(context).getToken(), true);
    }


}
