package co.hakim.hippam.domains.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import co.hakim.hippam.Constants;
import co.hakim.hippam.db.DBHelper;
import co.hakim.hippam.db.tables.CustomerTable;
import co.hakim.hippam.db.tables.SurveyTable;
import co.hakim.hippam.db.tables.TagihanTable;

/**
 * Created by Hakim on 28-Oct-16.
 *
 * @author by Hakim
 *         <p>
 *         Please update the author field if you are editing
 *         this file and your name is not written.
 */
public class HippamContentProviders extends ContentProvider {

    private static final String TAG = HippamContentProviders.class.getSimpleName();

    public static final String AUTHORITY = "co.hakim.hippam.providers";
    private static final int CUSTOMERS = 1;
    private static final int CUSTOMER_ID = 10;
    private static final int SURVEYS = 2;
    private static final int SURVEY_CUSTOMER = 20;
    private static final int SURVEYS_GROUP = 21;
    private static final int TAGIHAN = 3;

    private static final String BASE_CUSTOMERS = "customers";
    public static final Uri URI_CUSTOMERS = Uri.parse("content://" + AUTHORITY + "/" + BASE_CUSTOMERS);
    private static final String BASE_SURVEYS = "surveys";
    public static final Uri URI_SURVEYS = Uri.parse("content://" + AUTHORITY + "/" + BASE_SURVEYS);
    private static final String BASE_TAGIHAN = "tagihan";
    public static final Uri URI_TAGIHAN = Uri.parse("content://" + AUTHORITY + "/" + BASE_TAGIHAN);
    private static final String BASE_SURVEYS_GROUP = "surveys_group";
    public static final Uri URI_SURVEYS_GROUP = Uri.parse("content://" + AUTHORITY + "/" + BASE_SURVEYS_GROUP);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, BASE_CUSTOMERS, CUSTOMERS);
        uriMatcher.addURI(AUTHORITY, BASE_CUSTOMERS + "/#", CUSTOMER_ID);
        uriMatcher.addURI(AUTHORITY, BASE_SURVEYS, SURVEYS);
        uriMatcher.addURI(AUTHORITY, BASE_SURVEYS_GROUP, SURVEYS_GROUP);
        uriMatcher.addURI(AUTHORITY, BASE_SURVEYS + "/#", SURVEY_CUSTOMER);
        uriMatcher.addURI(AUTHORITY, BASE_TAGIHAN, TAGIHAN);
    }

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        int uriType = uriMatcher.match(uri);
        String groupBy = null;

        switch (uriType){
            case CUSTOMER_ID:
                queryBuilder.appendWhere(CustomerTable.COLUMN_CUSTOMER_ID + " = '" + uri.getLastPathSegment() + "'");
            case CUSTOMERS:
                queryBuilder.setTables(CustomerTable.TABLE_NAME);
                break;
            case SURVEY_CUSTOMER:
                queryBuilder.appendWhere(SurveyTable.COLUMN_CUSTOMER_ID + " = '" + uri.getLastPathSegment() + "'");
            case SURVEYS_GROUP:
                groupBy = "strftime('%Y-%m'," + SurveyTable.COLUMN_DATE + ")";
            case SURVEYS:
                queryBuilder.setTables(SurveyTable.TABLE_NAME);
                break;
            case TAGIHAN:
                queryBuilder.setTables(TagihanTable.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, groupBy,
                null, sortOrder);

        if (Constants.DEBUG)
            Log.d(TAG, "query: " + queryBuilder.buildQuery(
                    projection, selection, null, null, sortOrder, null));

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        boolean bypassInsert = false;
        int uriType = uriMatcher.match(uri);
        long id = 0;
        String basePath, tableName;
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        switch (uriType){
            case CUSTOMERS:
                basePath = BASE_CUSTOMERS;
                tableName = CustomerTable.TABLE_NAME;
                break;
            case SURVEYS:
                basePath = BASE_SURVEYS;
                tableName = SurveyTable.TABLE_NAME;
                break;
            case TAGIHAN:
                basePath = BASE_TAGIHAN;
                tableName = TagihanTable.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI for insert: " + uri);
        }

        if (!bypassInsert)
            id = database.insert(tableName, null, values);

        if (id < 0)
            return null;

        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.parse(basePath + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = uriMatcher.match(uri);
        int affected;
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String where = TextUtils.isEmpty(selection) ? "" : " and " + selection;

        switch (uriType){
            case CUSTOMER_ID :
                affected = database.delete(CustomerTable.TABLE_NAME,
                        CustomerTable.COLUMN_CUSTOMER_ID + " = " + uri.getLastPathSegment() + where, selectionArgs);
                break;
            case CUSTOMERS :
                affected = database.delete(CustomerTable.TABLE_NAME, selection, selectionArgs);
                break;
            case SURVEY_CUSTOMER :
                affected = database.delete(SurveyTable.TABLE_NAME,
                        SurveyTable.COLUMN_CUSTOMER_ID + " = " + uri.getLastPathSegment() + where, selectionArgs);
                break;
            case SURVEYS :
                affected = database.delete(SurveyTable.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI for delete: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return affected;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = uriMatcher.match(uri);
        int affected;
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String where = TextUtils.isEmpty(selection) ? "" : " and " + selection;

        if(Constants.DEBUG) Log.d(TAG, where.toString());

        switch (uriType){
            case CUSTOMER_ID :
                affected = database.update(CustomerTable.TABLE_NAME, values,
                        CustomerTable.COLUMN_CUSTOMER_ID + " = " + uri.getLastPathSegment() + where, selectionArgs);
                break;
            case CUSTOMERS :
                affected = database.update(CustomerTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SURVEY_CUSTOMER :
                affected = database.update(SurveyTable.TABLE_NAME, values,
                        SurveyTable.COLUMN_CUSTOMER_ID + " = " + uri.getLastPathSegment() + where, selectionArgs);
                break;
            case SURVEYS :
                affected = database.update(SurveyTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI for update: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return affected;
    }
}
